#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>
#include<prototypes.h>
#include<struct.h>

int main(void)
{
	int choice, exit = 0;

	do
	{
		system("cls");

		printf("Homework: C pointers\n\n");
		printf("Enter a number between 1 and 10 for the corresponding Problem: ");
		fflush(stdin);
		scanf("%d", &choice);
		switch (choice)
		{
			case 1: problem_1(); break;
			case 2: problem_2(); break;
			case 3: problem_3(); break;
			case 4: problem_4(); break;
			case 5: problem_5(); break;
			case 6: problem_6(); break;
			case 7: problem_7(); break;
			case 8: problem_8(); break;
			case 9: problem_9(); break;
			case 10: problem_10(); break;
			default: printf("\nIncorrect input."); break;
		}

		printf("\n\nPress ESCAPE to quit or press any other key to run another Problem.");
		exit = getch();

	} while (exit != 27); // Until ESCAPE is pressed.

	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	int testArray[] = {1, 2, 3, 4, 5};
	
	printf("\nProblem 1. Swap numbers.\n\n");

	printf("The original array is: ");
	print_int_array(testArray, 5); // The function is defined in the section of Problem 2 below.

	swap(&testArray[0], &testArray[1]);

	printf("\nThe array after the swap of its first two elements is: ");
	print_int_array(testArray, 5); // The function is defined in the section of Problem 2 below.
}

void swap(int *first, int *second)
{
	int temp;

	temp = *first;
	*first = *second;
	*second = temp;
}
//========================================================================================================
void problem_2(void)
{
	int testArray[] = {1, 2, 3, 4, 5};

	printf("\nProblem 2. Print an array.\n\n");

	print_int_array(testArray, 5);
}

void print_int_array(int *arrayX, int arrayLength)
{
	int i;

	for (i = 0; i < arrayLength; i++)
	{
		if (0 < i) printf(" ");
		printf("%d", *(arrayX + i));
	}
}
//========================================================================================================
void problem_3(void)
{
	int length, i;
	int *inputArray;

	printf("\nProblem 3. Print array reversed.\n\n");

	fflush(stdin);
	scanf("%d", &length);
	inputArray = (int*)calloc(length, sizeof(int));
	for (i = 0; i < length; i++) scanf("%d", &inputArray[i]);

	print_int_array_reversed(inputArray, length);

	free(inputArray);
}

void print_int_array_reversed(int *arrayX, int arrayLength)
{
	int i;

	for (i = arrayLength - 1; i >= 0; i--)
	{
		if (arrayLength - 1 > i) printf(" ");
		printf("%d", *(arrayX + i));
	}
}
//========================================================================================================
void problem_4(void)
{
	int number;

	printf("\nProblem 4. Print integer address.\n\n");

	fflush(stdin);
	scanf("%d", &number);
	
	printf("\nThe address of the integer variable is: %p", &number);
	print(number);
}

void print(int x)
{
	printf("\n\nThe address of the integer value passed as an input parameter to the function is: %p", &x);

	// This is not the address of the variable "number" declared in main().
	// When the function "print" is called, separate mamory is allocated for its input parameter and return address.
}
//========================================================================================================
void problem_5(void)
{
	int *p;

	printf("\nProblem 5. Create a new integer.\n\n");

	// FIRST METHOD:
	printf("The number created in new_integer() is: %d", new_integer());

	// SECOND METHOD:
	p = new_integer_ptr();
	printf("The number created in new_integer_ptr() is: %d", *p);

	// The SECOND METHOD is not reliable because it uses a memory address in the stack which,
	// after the end of the function new_integer_ptr(), becomes available for calls of other functions and variables.
}

int new_integer()
{
	int number;
	
	printf("Creating an integer number inside new_integer(): ");

	fflush(stdin);
	scanf("%d", &number);

	return number;
}
	
int* new_integer_ptr()
{
	int number;

	printf("\n\nCreating an integer number inside new_integer_ptr(): ");

	fflush(stdin);
	scanf("%d", &number);

	return &number;
}
//========================================================================================================
void problem_6(void)
{
	char originalText[1001];
	int replacedDigits = 0;

	printf("\nProblem 6. Replacing all digits with slashes.\n\n");

	fflush(stdin);
	gets(originalText);

	printf("\nThe original text is: %s", originalText);
	printf("\n\nThe modified text is: %s", replace_digits_in_text(originalText, &replacedDigits));
	printf("\n\nThe number of replaced digits is: %d", replacedDigits);
}

char* replace_digits_in_text(char* inputText, int* counter)
{
	int i;
	char *newText;

	newText = (char*)calloc(strlen(inputText), sizeof(char));

	for (i = 0; i < strlen(inputText); i++)
	{
		if (isdigit(*(inputText + i)))
		{
			*(newText + i) = '/';
			(*counter)++;
		}
		else
		{
			*(newText + i) = *(inputText + i);
		}
	}
	*(newText + strlen(inputText)) = '\0';
	return newText;
}

int isdigit(char x)
{
	if ((47 < x) && (58 > x)) return 1;
	else return 0;
}
//========================================================================================================
void problem_7(void)
{
	char *text = "I love to break free";
	int inputArray[] = { 7, 3, 2, 10, -5 };
	int size, i;

	printf("\nProblem 7. Generic memory dump function.\n\n");

	printf(text);
	printf("\n\n");
	mem_dump(text, strlen(text) + 1, 5);

	printf("\n\n");
	print_int_array(inputArray, 5);
	printf("\n\n");	
	size = sizeof(inputArray) / sizeof(int);
	mem_dump(inputArray, size * sizeof(int), 4);
}

void mem_dump(void *string, int size, int rowLength)
{
	int i;
	char *stringP = (char*)string;

	for (i = 0; i < size; i++)
	{
		if (0 == (i % rowLength))
		{
			if (0 < i) printf("\n");
			printf("0x%p", &stringP[i]);
		}

		printf(" %02x", stringP[i] & 0xFF);
	}
}
//========================================================================================================
void problem_8(void)
{
	char letter = 'B', symbol = '+';
	int a = 10, b = 6;
	double d = 3.14, f = 1.23567;

	printf("\nProblem 8. Generic swap function.\n\n");
	
	generic_swap(&letter, &symbol, sizeof(char));
	printf("CHAR: %c %c", letter, symbol);

	generic_swap(&a, &b, sizeof(int));
	printf("\n\nINT: %d %d", a, b);

	generic_swap(&d, &f, sizeof(double));
	printf("\n\nDOUBLE: %.2f %.2f", d, f);
}

void generic_swap(void *x, void *y, int size)
{
	int i;
	char temp;
	char *xP = (char*)x;
	char *yP = (char*)y;

	for (i = 0; i < size; i++)
	{
		temp = xP[i];
		xP[i] = yP[i];
		yP[i] = temp;
	}
}
//========================================================================================================
void problem_9(void)
{
	
	// struct Client is defined in struct.h.
	int clientsCount = 10;




	printf("\nProblem 9. Clients.\n\n");

	sort_clients(clients, clientsCount, &name_comparator);
	sort_clients(clients, clientsCount, &balance_comparator);
	sort_clients(clients, clientsCount, &age_comparator);	
	
}

void sort_clients(struct Client clients, int count, void (*comparator)(struct Client))
{

}

void name_comparator(struct Client clients)
{

}

void balance_comparator(struct Client clients)
{

}

void age_comparator(struct Client clients)
{

}
//========================================================================================================
void problem_10(void)
{

}
//========================================================================================================
