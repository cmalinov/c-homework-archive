

void swap(int *first, int *second);
void print_int_array(int *arrayX, int arrayLength);
void print_int_array_reversed(int *arrayX, int arrayLength);
void print(int x);
char* replace_digits_in_text(char* inputText, int* replacements);
int isdigit(char x);
int new_integer();
int* new_integer_ptr();
void some_other_function();
void mem_dump(char *string, int size, int rowLength);
void generic_swap(void *x, void *y, int size);

void sort_clients(struct Client *clients, int count, void (*comparator)(struct Client));
void name_comparator(struct Client *clients);
void balance_comparator(struct Client *clients);
void age_comparator(struct Client *clients);

void problem_1(void);
void problem_2(void);
void problem_3(void);
void problem_4(void);
void problem_5(void);
void problem_6(void);
void problem_7(void);
void problem_8(void);
void problem_9(void);
void problem_10(void);



