
void reverse_string(char *s);
void filter_text(char *text, char *searchedWord, char replacement);

void string_copy(char *buffer, char *text, int length);
void string_cut(char *text, int length);
void string_cat(char *buff, char* newText, int n);
int wc(char * input, char delimiter);
int string_len(char *text);
int strsearch(char * src, char * substr);
void substr(char * src, int position, int length);
char * read_line(void);

void problem_1(void);
void problem_2(void);
void problem_3(void);
void problem_4(void);
void problem_5(void);
void problem_6(void);
void problem_7(void);
void problem_8(void);
void problem_9(void);
void problem_10(void);
void problem_11(void);
void problem_12(void);
void problem_13(void);
void problem_14(void);
void problem_15(void);
void problem_16(void);
void problem_17(void);
void problem_18(void);


