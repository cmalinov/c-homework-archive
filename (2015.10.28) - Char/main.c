#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C characters and strings\n\n");
	printf("Enter a number between 1 and 18 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem_1(); break;
		case 2: problem_2(); break;
		case 3: problem_3(); break;
		case 4: problem_4(); break;
		case 5: problem_5(); break;
		case 6: problem_6(); break;
		case 7: problem_7(); break;
		case 8: problem_8(); break;
		case 9: problem_9(); break;
		case 10: problem_10(); break;
		case 11: problem_11(); break;
		case 12: problem_12(); break;
		case 13: problem_13(); break;
		case 14: problem_14(); break;
		case 15: problem_15(); break;
		case 16: problem_16(); break;
		case 17: problem_17(); break;
		case 18: problem_18(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	char inputString[1000];

	printf("\nProblem 1. Reverse a string.\n\n");

	fflush(stdin);
	scanf("%s", &inputString);

	reverse_string(inputString);
	printf("%s", inputString);
}

void reverse_string(char *s)
{
	int i;
	char temp;

	for (i = 0; i < strlen(s) / 2; i++)
	{
		temp = s[i];
		s[i] = s[strlen(s) - 1 - i];
		s[strlen(s) - 1 - i] = temp;
	}
}
//========================================================================================================
void problem_2(void)
{
	char text[1000];
	int i, limit = 20;

	printf("\nProblem 2. String length.\n\n");

	fflush(stdin);
	gets(text);

	if (limit > strlen(text))
	{
		for (i = strlen(text); i < limit; i++)
		{
			text[i] = '*';
		}
	}
	text[20] = '\0';

	printf("%s", text);
}
//========================================================================================================
void problem_3(void)
{
	char inputText[1000];
	char outputText[1000];
	char c = '\0';
	int i, j = 0;

	printf("\nProblem 3. Series of letters.\n\n");

	fflush(stdin);
	gets(inputText);

	for (i = 0; i < strlen(inputText); i++)
	{
		if (c != inputText[i])
		{
			outputText[j++] = c = inputText[i];
		}
	}
	outputText[j] = '\0';

	printf("%s", outputText);
}
//========================================================================================================
void problem_4(void)
{
	char inputText[1000];
	char searchedText[1000];
	int i, j, counter = 0, match;

	printf("\nProblem 4. Count substring occurrences.\n\n");

	fflush(stdin);
	gets(inputText);
	fflush(stdin);
	gets(searchedText);

	if (strlen(inputText) < strlen(searchedText))
	{
		printf("0");
		return;
	}

	for (i = 0; i < strlen(inputText) - strlen(searchedText) + 1; i++)
	{
		match = 1;
		for (j = 0; j < strlen(searchedText); j++)
		{
			if (searchedText[j] != inputText[i + j])
			{
				match = 0;
			}
		}
		if (1 == match) counter++;
	}
	printf("%d", counter);
}
//========================================================================================================
void problem_5(void)
{
	char inputText[2000];
	char bannedList[2000];
	char bannedWords[30][50];
	int temp, bannedWordsLengths[30]; // These variables will be used in sorting of the list of words.
	char tempS[50];
	int bannedWordsCount = 0;
	int i, j = 0, wordEnd = 0;

	printf("\nProblem 5. Text filter.\n\n");

	fflush(stdin);
	gets(bannedList);
	fflush(stdin);
	gets(inputText);

	// FIRST: Recognition of the banned words.
	for (i = 0; i <= strlen(bannedList); i++)
	{
		if ((',' != bannedList[i]) && ('\0' != bannedList[i])) // The comma is always an invalid character.
		{
			if (!((' ' == bannedList[i]) && (1 == wordEnd))) // The interval is a valid character if it is not preceeded by a comma.
			{
				bannedWords[bannedWordsCount][j++] = bannedList[i];
			}
			wordEnd = 0;
		}
		else if ((',' == bannedList[i]) || ('\0' == bannedList[i]))
		{
			if (',' == bannedList[i]) wordEnd = 1; // This flag informs that the next interval character is not a part of a key word.

			bannedWords[bannedWordsCount][j] = '\0';
			bannedWordsLengths[bannedWordsCount] = j - 1;
			bannedWordsCount++;
			j = 0;
		}
	}
	
	// SECOND: Sorting of the words so that the longest ones will be replaced first.
	for (i = 0; i < bannedWordsCount; i++)
	{
		for (j = i; j < bannedWordsCount; j++)
		{
			if (bannedWordsLengths[i] < bannedWordsLengths[j])
			{
				temp = bannedWordsLengths[i];
				bannedWordsLengths[i] = bannedWordsLengths[j];
				bannedWordsLengths[j] = temp;

				strcpy(tempS, bannedWords[i]);
				strcpy(bannedWords[i], bannedWords[j]);
				strcpy(bannedWords[j], tempS);
			}
		}
	}

	// THIRD: Replacment of the banned words.
	for (i = 0; i < bannedWordsCount; i++)
	{
		filter_text(inputText, bannedWords[i], '*');
	}

	// OUTPUT
	printf("%s", inputText);
}

void filter_text(char *text, char *searchedWord, char replacement)
{
	int i, j, match;

	if (strlen(text) < strlen(searchedWord))
	{
		return;
	}

	// SEARCHING AND REPLACING
	for (i = 0; i < strlen(text) - strlen(searchedWord) + 1; i++)
	{
		// FINDING
		match = 1;
		for (j = 0; j < strlen(searchedWord); j++)
		{
			if (searchedWord[j] != text[i + j])
			{
				match = 0;
			}
		}

		// REPLACING
		if (1 == match)
		{
			for (j = 0; j < strlen(searchedWord); j++)
			{
				text[i + j] = replacement;
			}
		}
	}
}
//========================================================================================================
void problem_6(void)
{
	char text[1000];

	printf("\nProblem 6. Palindromes.\n\n");

	fflush(stdin);
	gets(text);

	// ALGORITHM:
	// 1) Go through the text.
	// 2) Find a word.
	// 3) Check whether it is a palindrome.
	// 4) If it is a palindrome and the word never found before, put it in the list.
	// 5) Repeat the previous steps until '\0' is reached.
	// 6) Sort the list of palindromes lexicgraphically.
	// 7) Print the list.

}
//========================================================================================================
void problem_7(void)
{
	char inputText[1000];
	char outputText[1000];
	int size, i;

	printf("\nProblem 7. Implament a string copy function.\n\n");

	printf("Enter the text: ");
	fflush(stdin);
	gets(inputText);
	printf("Enter the length (number of characest to be cut): ");
	fflush(stdin);
	scanf("%d", &size);

	string_copy(outputText, inputText, size);
	printf("The new output string is: %s", outputText);

	string_cut(inputText, size);
	printf("\nThe cut input string is: %s", inputText);
}

void string_copy(char *buffer, char *text, int length)
{
	int i;

	for (i = 0; i < length; i++)
	{
		buffer[i] = text[i];
	}
	buffer[i] = '\0';
}

void string_cut(char *text, int length)
{
	text[length] = '\0';
}
//========================================================================================================
void problem_8(void)
{
	char buffer[] = "Soft";
	char *b;

	printf("\nProblem 8. Implement a string concatenation function.\n\n");

	b = (char*)malloc(sizeof(char) * strlen(buffer));
	strcpy(b, buffer);

	printf("BEFORE: %s", b);
	string_cat(b, "ware University", 15);
	printf("\nAFTER: %s", b);

	free(b);
}

void string_cat(char *buff, char* newText, int n)
{
	int i, j = 0, initialSize = strlen(buff);

	buff = (char*)realloc(buff, sizeof(char) * (initialSize + n + 1));
	if (!buff)
	{
		printf("\n The needed memory cannot be allocated");
		return;
	}

	for (i = initialSize; i < initialSize + n; i++)
	{
		buff[i] = newText[j++];
	}
	buff[i] = '\0';
}
//========================================================================================================
void problem_9(void)
{
	printf("\nProblem 9. Implement a word count function.\n\n");

	wc("Hard Rock, Hallelujah!", ' ');
	wc("Hard Rock, Hallelujah!", ',');
	wc("Uncle Sam wants you Man", ' ');
	wc("Beat the beat!", '!');
	wc("!!", '!');
}

int wc(char * input, char delimiter)
{
	int i, counter = 1;

	for ( i = 0; i < strlen(input); i++)
	{
		if (delimiter == input[i]) counter++;
	}

	printf("\n%d", counter);
	return counter;
}
//========================================================================================================
void problem_10(void)
{
	char buffer1[10] = { 'C', '\0', 'B', 'a', 'b', 'y' };
	char buffer2[] = { 'D', 'e', 'r', 'p', '\0' };

	printf("\nProblem 10. Implement a string length function.\n\n");

	string_len("Soft");
	string_len("SoftUni");
	string_len(buffer1);
	string_len(buffer2);
}

int string_len(char *text)
{
	int i = 0, counter = 0;

	do
	{
		if ('\0' != text[i]) counter++;
	} while ('\0' != text[i++]);

	printf("\n%d", counter);
	return counter;
}
//========================================================================================================
void problem_11(void)
{
	printf("\nProblem 11. Implement a string search function.\n\n");

	strsearch("SoftUni", "Soft");
	strsearch("Hard Rock", "Rock");
	strsearch("Ananas", "nasa");
	strsearch("Coolness", "cool");
}

int strsearch(char *src, char *substr)
{
	int i, j, match;

	if (strlen(src) < strlen(substr))
	{
		printf("\n0");
		return 0;
	}

	// SEARCHING
	for (i = 0; i < strlen(src) - strlen(substr) + 1; i++)
	{
		// FINDING
		match = 1;
		for (j = 0; j < strlen(substr); j++)
		{
			if (substr[j] != src[i + j])
			{
				match = 0;
			}
		}

		if (1 == match) // FOUND
		{
			printf("\n1");
			return 1;
		}
	}

	printf("\n0"); // NOT FOUND
	return 0;
}
//========================================================================================================
void problem_12(void)
{
	printf("\nProblem 12. Implement a substring function.\n\n");

	substr("Breaking Bad", 0, 2);
	substr("Maniac", 3, 3);
	substr("Maniac", 3, 5);
	substr("Master Yoda", 13, 5);
}

void substr(char * src, int position, int length)
{
	int i;

	if (position >= strlen(src)) printf("\n(empty string)");
	else
	{
		printf("\n");
		for (i = position; i < ((strlen(src) < (position + length))?strlen(src):(position + length)); i++)
		{
			printf("%c", src[i]);
		}
	}
}
//========================================================================================================
void problem_13(void)
{
	char *text;

	printf("\nProblem 13. Read line function.\n\n");

	text = read_line();
	printf("\n%s", text);

	free(text);
}

char * read_line(void)
{
	int i = 0;
	char c;
	char *p;
	int size = 20 * sizeof(char), currentSize = size;

	p = (char *)malloc(size);

	if (!p)
	{
		printf("The memory neccessary cannot be allocated.");
		return NULL;
	}

	fflush(stdin);
	do
	{
		// Allocation of more memory if the currently allocated is consumed.
		if ((i + 1) == currentSize)
		{
			p = (char *)realloc(p, currentSize + size);
			if (!p)
			{
				printf("The memory neccessary cannot be allocated.");
				return p;
			}
			currentSize += size;
		}

		c = getc(stdin);

		if (('\n' == c) || (EOF == c)) p[i] = '\0';
		else p[i++] = c;
	} while (('\n' != c) && (EOF != c));

	// The allocated memory is cut back to the size of the string.
	// No unnecessary memory is allocated.
	p = (char *)realloc(p, (i + 1) * sizeof(char));
	if (!p)
	{
		printf("realloc error while shrinking the allocated memory.");
		return p;
	}

	return p;
}
//========================================================================================================
void problem_14(void)
{

}
//========================================================================================================
void problem_15(void)
{

}
//========================================================================================================
void problem_16(void)
{

}
//========================================================================================================
void problem_17(void)
{

}
//========================================================================================================
void problem_18(void)
{

}
//========================================================================================================

