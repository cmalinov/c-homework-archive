#include<stdio.h>
#include<stdlib.h>

int board(double x, double y);
int head(double x, double y);
int arm(double x, double y);

double boardX, boardY, boardRadius;
double headX, headY, headRadius;
double topLeftX, topLeftY, bottomRightX, bottomRightY;

int main(void)
{
	int n = 1;
	double shotX[1000], shotY[1000];
	int i, points = 0, health = 100, ratio = 0, hits = 0;
	double ratioD = 0.0;
	
	// INPUT
	fflush(stdin);
	scanf("%lf%lf%lf", &boardX, &boardY, &boardRadius);
	scanf("%lf%lf%lf", &headX, &headY, &headRadius);
	scanf("%lf%lf%lf%lf", &topLeftX, &topLeftY, &bottomRightX, &bottomRightY);
	scanf("%d", &n);
	for (i = 0; i < n; i++)
	{	
		scanf("%lf%lf", &shotX[i], &shotY[i]);
	}
	
	
	// CALCULATION
	for (i = 0; i < n; i++)
	{
		// IF BOARD
		if (1 == board(shotX[i], shotY[i]))
		{
			points += 50;
			hits++;

			// IF BOARD && (HEAD OR ARM)
			if ((1 == head(shotX[i], shotY[i])) || (1 == arm(shotX[i], shotY[i])))
			{
				points -= 25;
			}
		}

		// IF HEAD
		if (1 == head(shotX[i], shotY[i]))
		{
			health -= 25;
		}

		// IF ARM
		if (1 == arm(shotX[i], shotY[i]))
		{
			health -= 30;
		}

		// RATIO
		ratioD = (((double)hits) / (i + 1)) * 100;
		//ratioD = ratioD - 0.5;
		ratio = (int)ratioD;

		if (0 >= health)
		{
			health = 0;
			break;
		}
	}

	// OUTPUT
	printf("Points: %d\nHit ratio: %d%%\nBay Mile: %d", points, ratio, health); 
	//printf("\nhits = %d", hits);
	return 0;
}

int board(double x, double y)
{
	int result = 0;

	if (((x - boardX) * (x - boardX) + (y - boardY) * (y - boardY)) <= (boardRadius * boardRadius)) result = 1;

	return result;
}

int head(double x, double y)
{
	int result = 0;

	if (((x - headX) * (x - headX) + (y - headY) * (y - headY)) <= (headRadius * headRadius)) result = 1;

	return result;
}

int arm(double x, double y)
{
	int result = 0;

	// topLeftX, topLeftY, bottomRightX, bottomRightY
	if ((y <= topLeftY) && (y >= bottomRightY) && (x >= topLeftX) && (x <= bottomRightX)) result = 1;

	return result;
}