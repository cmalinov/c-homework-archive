#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char * read_line(void);

int main(void)
{
	int i, exit = 0;

	char *input; // command line
	char *output; // final result - the resulting string
	int outputSize = 0; // the size of the memory allocated for the final result
	char *tempPointer;
	char *remainder;
	int insertIndex = 0;
	char *tempString;
	int insertLength;
	int nextReplace = 0;

	char occurrenceText[200];
	char replacementText[200];

	
	output = (char*)malloc(1);
	if (!output) return -1;
	output[0] = '\0';
	outputSize = 1;
	

	do
	{
		// INPUT
		input = read_line();


		// PROCESSING
		if (input == strstr(input, "concat-")) // concat-{string} 
		//if ('c' == input[0])
		{
			outputSize += strlen(input + 7);
			output = (char*)realloc(output, outputSize * sizeof(char));

			strcat(output, input + 7); // PART1 + PART2

			outputSize = strlen(output) + 1;
		}
		else if (input == strstr(input, "insert-")) // insert-{string}-{pos}
		//else if ('i' == input[0])
		{
			// The original TEXT will be split into 2 parts: PART1 and PART2.
			// STRING will be placed between PART1 and PART2.
			// Result: PART1 + STRING + PART2.

			tempPointer = strrchr(input, '-');
			insertIndex = strtol(tempPointer + 1, &remainder, 10);
			*tempPointer = '\0'; // => insert-{string}'\0'{pos}

			insertLength = strlen(input + 7); // length STRING

			tempString = (char*)malloc(outputSize);
			strcpy(tempString, output + insertIndex); // saving PART2

			output = (char*)realloc(output, (outputSize + insertLength) * sizeof(char));
			
			// Connecting PART1 and STRING
			for (i = 0; i < insertLength; i++)
			{
				output[insertIndex + i] = *(input + 7 + i);
			}
			output[insertIndex + i] = '\0';

			// Connecting PART1_STRING and PART2.
			strcat(output, tempString);

			outputSize = strlen(output) + 1;

			free(tempString);
		}
		else if (input == strstr(input, "replace-")) // replace-{occurrence}-{replacement}
		//else if ('r' == input[0])
		{
			tempPointer = strrchr(input, '-');
			*tempPointer = '\0'; // => replace-{occurrence}'\0'{replacement}
			strcpy(occurrenceText, input + 8);
			strcpy(replacementText, tempPointer + 1);

			nextReplace = 0;
			while(1)
			{
				tempPointer = strstr(output + nextReplace, occurrenceText); // text1_occurence_text2
				if (NULL != tempPointer)
				{
					nextReplace = tempPointer - output;

					tempString = (char*)malloc(outputSize);
					strcpy(tempString, tempPointer + strlen(occurrenceText)); // saving PART2
					
					if (strlen(occurrenceText) < strlen(replacementText))
					{
						outputSize +=  strlen(replacementText) - strlen(occurrenceText);
						output = (char*)realloc(output, outputSize * sizeof(char));
					}
										
					output[nextReplace] = '\0';
					strcat(output, replacementText);
					strcat(output, tempString);

					outputSize = strlen(output) + 1;

					free(tempString);

					nextReplace += strlen(replacementText);
				}
				else break;
			}

		}
		else if (0 == strcmp(input, "over"))
		//else if ('o' == input[0])
		{
			exit = 1;
		}
		else
		{
			// Incorrect input.
		}

		free(input);
	} while (0 == exit);

	printf("%s", output);

	free(output);
}



char * read_line(void)
{
	int i = 0;
	char c;
	char *p;
	int size = 256;

	p = (char *)malloc(size * sizeof(char));
	if (!p)
	{
		printf("The memory neccessary cannot be allocated.");
		return NULL;
	}


	do
	{
		// Allocation of more memory if the currently allocated is consumed.
		if ((i + 1) == size)
		{
			size += 256;
			p = (char *)realloc(p, size);
			if (!p)
			{
				printf("The memory neccessary cannot be allocated.");
				return p;
			}
		}

		//c = getc(stdin);
		c = getchar();

		if (('\n' == c) || (EOF == c)) p[i] = '\0';
		else p[i++] = c;
	} while (('\n' != c) && (EOF != c));

	return p;
}


