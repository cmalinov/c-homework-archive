#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>



int main(int argc, char** argv)
{
	char nameSrc[500], nameDest[500];
	FILE *fileSrc, *fileDest;
	char *format;

	char buffer[65];
	int block = 64;
	int readBytes;

	int i;

	
	if (2 > argc)
	{
		printf("Usage: [<src-file-1> <src-file-2> �]");
		exit(EXIT_FAILURE);
	}

	format = strrchr(argv[1], '.');
	strcpy(nameDest, "merged");
	strcat(nameDest, format);

	errno = 0;
	fileDest = fopen(nameDest, "wb");
	if (!fileDest)
	{
		printf("The file %s is not opened for writing.", nameDest);
		exit(EXIT_FAILURE);
	}

	for (i = 1; i < argc; i++)
	{
		strcpy(nameSrc, argv[i]);

		errno = 0;
		fileSrc = fopen(nameSrc, "rb");
		if (!fileSrc)
		{
			if (ENOENT == errno) printf("%s: No such file or directory", nameSrc);
			exit(EXIT_FAILURE);
		}

		// EXTRACTING DATA FROM THE SOURCE FILES AND MERGING IT IN THE DESTINATION FILE:
		while (!(feof(fileSrc)))
		{
			// Take the next block
			readBytes = fread(buffer, 1, block, fileSrc);
			buffer[readBytes] = '\0';

			if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "3456")) // Apple
			{
				// Move this block's content to the destinationi file.
				fwrite(buffer, 1, readBytes - 4, fileDest);
			}
			else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "!\"#$")) // Leaf
			{
				// Move the first half of this block's content to the destinationi file.
				fwrite(buffer, 1, (readBytes - 4) / 2, fileDest);
			}
			else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "+,-.")) // Cross
			{
				// SKIP THE CROSS
			}
			else
			{
				if (0 != strlen(buffer))
				{
					printf("\nThe source file's content is incorrect.");
					printf("\nfile: %s", nameSrc);
					printf("\nbuffer: %s", buffer);
				}
			}
		}

		fclose(fileSrc);
	}

	
	fclose(fileDest);

	printf("Eureka!");

	return (EXIT_SUCCESS);
}





/*
int main(int argc, char** argv)
{
	char nameSrc1[500], nameSrc2[500], nameDest[500];
	//char *nameSrc, *nameDest;
	FILE *fileSrc1, *fileSrc2, *fileDest;

	char buffer[65];

	int block = 64;
	int readBytes;






	strcpy(nameSrc1, "src1.txt");
	strcpy(nameSrc2, "src2.txt");
	strcpy(nameDest, "dest.txt");


	
	errno = 0;
	fileSrc1 = fopen(nameSrc1, "rb");
	if (!fileSrc1)
	{
		if (ENOENT == errno) printf("%s: No such file or directory", nameSrc1);
		exit(EXIT_FAILURE);
	}

	errno = 0;
	fileSrc2 = fopen(nameSrc2, "rb");
	if (!fileSrc2)
	{
		if (ENOENT == errno) printf("%s: No such file or directory", nameSrc2);
		exit(EXIT_FAILURE);
	}

	errno = 0;
	fileDest = fopen(nameDest, "wb");
	if (!fileDest)
	{
		printf("The file %s is not opened for writing.", nameDest);
		fclose(fileSrc1);
		fclose(fileSrc2);
		exit(EXIT_FAILURE);
	}
	
	

	// EXTRACTING DATA FROM THE SOURCE FILES AND MERGING IT IN THE DESTINATION FILE:
	while (!(feof(fileSrc1)))
	{
		// Take the next block
		readBytes = fread(buffer, 1, block, fileSrc1);
		buffer[readBytes] = '\0';

		//if (64 > readBytes) // If tha last block is not full
		{
			printf("\nbuffer = %s", buffer);
		}
		printf("\nbuffer[0] = %c", buffer[0]);
		printf("\nbuffer[1] = %c", buffer[1]);
		printf("\nbuffer[2] = %c", buffer[2]);
		printf("\nbuffer[3] = %c\n\n", buffer[3]);

		if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "3456")) // Apple
		{
			// Move this block's content to the destinationi file.
			fwrite(buffer, 1, readBytes - 4, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "!\"#$")) // Leaf
		{
			// Move the first half of this block's content to the destinationi file.
			fwrite(buffer, 1, (readBytes - 4) / 2, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "+,-.")) // Cross
		{
			// SKIP THE CROSS
		}
		else
		{
			printf("The source file's content is incorrect.");
		}
	}
	
	// EXTRACTING DATA FROM THE SOURCE FILES AND MERGING IT IN THE DESTINATION FILE:
	while (!(feof(fileSrc2)))
	{
		// Take the next block
		readBytes = fread(buffer, 1, block, fileSrc2);
		buffer[readBytes] = '\0';

		//if (64 > readBytes) // If tha last block is not full
		{
			printf("\nbuffer = %s", buffer);
		}
		printf("\nbuffer[0] = %c", buffer[0]);
		printf("\nbuffer[1] = %c", buffer[1]);
		printf("\nbuffer[2] = %c", buffer[2]);
		printf("\nbuffer[3] = %c\n\n", buffer[3]);

		if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "3456")) // Apple
		{
			// Move this block's content to the destinationi file.
			fwrite(buffer, 1, readBytes - 4, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "!\"#$")) // Leaf
		{
			// Move the first half of this block's content to the destinationi file.
			fwrite(buffer, 1, (readBytes - 4) / 2, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "+,-.")) // Cross
		{
			// SKIP THE CROSS
		}
		else
		{
			printf("The source file's content is incorrect.");
		}
	}


	fclose(fileSrc1);
	fclose(fileSrc2);
	fclose(fileDest);

	printf("Success!");

	return (EXIT_SUCCESS);
}












int main(int argc, char** argv)
{
	char nameSrc[500], nameDest[500];
	//char *nameSrc, *nameDest;
	FILE *fileSrc, *fileDest;

	char buffer[65];

	int block = 64;
	int readBytes;





	strcpy(nameSrc, "src.txt");
	strcpy(nameDest, "dest.txt");


	
	errno = 0;
	fileSrc = fopen(nameSrc, "rb");
	if (!fileSrc)
	{
		if (ENOENT == errno) printf("%s: No such file or directory", nameSrc);
		exit(EXIT_FAILURE);
	}

	errno = 0;
	fileDest = fopen(nameDest, "wb");
	if (!fileDest)
	{
		printf("The file %s is not opened for writing.", nameDest);
		fclose(fileSrc);
		exit(EXIT_FAILURE);
	}
	
	
	// EXTRACTING DATA FROM THE SOURCE FILE:
	while (!(feof(fileSrc)))
	{
		// Take the next block
		readBytes = fread(buffer, 1, block, fileSrc);
		buffer[readBytes] = '\0';

		//if (64 > readBytes) // If tha last block is not full
		{
			printf("\nbuffer = %s", buffer);
		}
		printf("\nbuffer[0] = %c", buffer[0]);
		printf("\nbuffer[1] = %c", buffer[1]);
		printf("\nbuffer[2] = %c", buffer[2]);
		printf("\nbuffer[3] = %c\n\n", buffer[3]);

		if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "3456")) // Apple
		{
			// Move this block's content to the destinationi file.
			fwrite(buffer, 1, readBytes - 4, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "!\"#$")) // Leaf
		{
			// Move the first half of this block's content to the destinationi file.
			fwrite(buffer, 1, (readBytes - 4) / 2, fileDest);
		}
		else if ((buffer + readBytes - 4) == strstr((buffer + readBytes - 4), "+,-.")) // Cross
		{
			// SKIP THE CROSS
		}
		else
		{
			printf("The source file's content is incorrect.");
		}
	}

	fclose(fileSrc);
	fclose(fileDest);

	printf("Success!");

	return (EXIT_SUCCESS);
}

*/

