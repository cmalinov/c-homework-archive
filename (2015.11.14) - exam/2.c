#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int move(int movement);
//typedef enum { LEFT, RIGHT, UP, DOWN } direction;
//direction movement;
int N;
unsigned int numbers[20];
int row = 0, column = 0;

int main(void)
{
	int i, end = 0, mine = 0, x = 0;
	char command[6]; //  left, right, up, down


	// INPUT
	fflush(stdin);
	scanf("%d", &N);
	for (i = 0; i < N; i++)
	{
		scanf("%d", &numbers[i]);
	}

	// GAME
	do
	{
		//fflush(stdin);
		while ((EOF != x) && ('\n' != x))
		{
			x = getchar();
		}

		scanf("%s", &command);
		
		//if (0 == strcmp(command, "left"))
		if ('l' == command[0])
		{
			//movement = LEFT;
			mine = move(0);
		}
		//else if (0 == strcmp(command, "right"))
		else if ('r' == command[0])
		{
			//movement = RIGHT;
			mine = move(1);
		}
		//else if (0 == strcmp(command, "up"))
		else if ('u' == command[0])
		{
			//movement = UP;
			mine = move(2);
		}
		//else if (0 == strcmp(command, "down"))
		else if ('d' == command[0])
		{
			//movement = DOWN;
			mine = move(3);
		}
		else if ('e' == command[0])//if (0 == strcmp(command, "end"))
		{
			end = 1;
		}
	} while ((1 != end) && (1 != mine));

	// OUTPUT
	if (1 == mine)
	{
		printf("GAME OVER. Stepped a mine at %d %d\n", row, column);
	}
	for (i = 0; i < N; i++)
	{
		if (0 < i) printf("\n");
		printf("%u", numbers[i]);
	}

	return 0;
}

int  move(int movement)
{
	int mask = 1, lastRow, lastColumn;

	lastRow = row;
	lastColumn = column;

	// MOVE
	//if (LEFT == movement)
	if (0 == movement)
	{
		if (31 == column) column = 0;
		else column++;
	}
	//else if (RIGHT == movement)
	else if (1 == movement)
	{
		if (0 == column) column = 31;
		else column--;
	}
	//else if (UP == movement)
	else if (2 == movement)
	{
		if (0 == row) row = N - 1;
		else row--;
	}
	//else if (DOWN == movement)
	else if (3 == movement)
	{
		if ((N - 1) == row) row = 0;
		else row++;
	}

	// Check for a mine.
	mask = 1;
	mask <<= column;
	if (0 != (numbers[row] & mask))
	{
		return 1;
	}

	// The value at the previous position becomes 0.
	mask = 1;
	mask <<= lastColumn;
	numbers[lastRow] ^= mask;

	// The value at the current position becomes 1.
	mask = 1;
	mask <<= column;
	numbers[row] |= mask;

	return 0;
}
