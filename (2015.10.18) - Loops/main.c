#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C Loops\n\n");
	printf("Enter a number between 1 and 19 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem1(); break;
		case 2: problem2(); break;
		case 3: problem3(); break;
		case 4: problem4();	break;
		case 5: problem5(); break;
		case 6: problem6(); break;
		case 7: problem7(); break;
		case 8: problem8(); break;
		case 9: problem9(); break;
		case 10: problem10(); break;
		case 11: problem11(); break;
		case 12: problem12(); break;
		case 13: problem13(); break;
		case 14: problem14(); break;
		case 15: problem15(); break;
		case 16: problem16(); break;
		case 17: problem17(); break;
		case 18: problem18(); break;
		case 19: problem19(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem1(void)
{
	int n, i;

	printf("\nProblem 1. Numbers from 1 to n.");
	printf("\n\nEnter a positive integer number: n = ");
	scanf("%d", &n);

	printf("Result:");
	for (i = 1; i <= n; i ++)
	{
		printf(" %d", i);
	}
}
//========================================================================================================
void problem2(void)
{
	int n, i;

	printf("\nProblem 2. Numbers not divisible by 3 and 7.");
	printf("\n\nEnter a positive integer number: n = ");
	scanf("%d", &n);

	printf("Result:");
	for (i = 1; i <= n; i ++)
	{
		if ((0 != (i % 3)) && (0 != (i % 7)))
		{
			printf(" %d", i);
		}
	}
}
//========================================================================================================
void problem3(void)
{
	int n, i;
	int *numbers, min, max, sum = 0;
	float average;

	printf("\nProblem 3. Min, Max, Sum and Average of n numbers.");
	printf("\n\nEnter the value of n followed by n integer numbers:\n");
	scanf("%d", &n);
	numbers = (int*)calloc(n, sizeof(int));

	for (i = 0; i < n; i++)
	{
		scanf("%d", &numbers[i]);

		if (0 == i)
		{
			min = numbers[i];
			max = numbers[i];
		}
		else
		{
			if (numbers[i] < min) min = numbers[i];
			if (numbers[i] > max) max = numbers[i];
		}

		sum += numbers[i];
	}
	
	average = (float)sum / n;

	printf("min = %.2f\n", (float)min);
	printf("max = %.2f\n", (float)max);
	printf("sum = %.2f\n", (float)sum);
	printf("avg = %.2f", average);
}
//========================================================================================================
void problem4(void)
{
	int i, j;
	char rank, suit;

	printf("\nProblem 4. Print a deck of 52 cards.\n\n");

	for (i = 2; i < 15; i++)
	{
		for (j = 1; j <= 4; j++)
		{
			switch (i)
			{
				case 11: rank = 'J'; break;
				case 12: rank = 'Q'; break;
				case 13: rank = 'K'; break;
				case 14: rank = 'A'; break;
				default: break;
			}

			switch (j)
			{
				case 1: suit = 'S'; break;
				case 2: suit = 'H'; break;
				case 3: suit = 'C'; break;
				case 4: suit = 'D'; break;
			}
			
			if (1 == j)
			{
				if (2 < i) printf("\n");
			}
			else printf(" ");

			if (i < 11) printf("%d%c", i, suit);
			else printf("%c%c", rank, suit);
		}
	}
}
//========================================================================================================
void problem5(void)
{
	int n, x, i;
	double sum = 1.0, numerator = 1.0, denominator = 1.0;

	printf("\nProblem 5. Calculate S = 1 + 1!/x^1 + 2!/x^2 + ... + n!/x^n.");
	printf("\n\nn = ");
	scanf("%d", &n);
	printf("x = ");
	scanf("%d", &x);

	for (i = 1; i <= n; i++)
	{
		numerator *= i;
		denominator *= x;
		sum += numerator / denominator;
	}

	printf("S = %.5lf", sum);
}
//========================================================================================================
void problem6(void)
{
	int n, k, i;
	double numerator = 1.0, denominator = 1.0;

	printf("\nProblem 6. Calculate n!/k!.");
	printf("\n\n(2 < n < 100) Enter n = ");
	scanf("%d", &n);
	printf("(0 < k < n) Enter k = ");
	scanf("%d", &k);

	if ((2 < n) && (100 > n) && (0 < k) && (k < n))
	{
		for (i = 1; i <= n; i++)
		{
			numerator *= i;
			if (i <= k) denominator *= i;
		}

		printf("n!/k! = %.0lf", numerator / denominator);
	}
	else
	{
		printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem7(void)
{
	int n, k, i;
	double numerator = 1.0; // To be used for the calculation of n!
	double denominatorPart1 = 1.0; // To be used for the calculation of k!
	double denominatorPart2 = 1.0; // To be used for the calculation of (n - k)!

	printf("\nProblem 7. Calculate n! / (k! * (n - k)!).");
	printf("\n\n(2 < n < 100) Enter n = ");
	scanf("%d", &n);
	printf("(0 < k < n) Enter k = ");
	scanf("%d", &k);

	if ((2 < n) && (100 > n) && (0 < k) && (k < n))
	{
		for (i = 1; i <= n; i++)
		{
			numerator *= i; // Calculation of n!
			if (i <= k) denominatorPart1 *= i; // Calculation of k!
			if (i <= (n - k)) denominatorPart2 *= i; // Calculation of (n - k)!
		}

		printf("n! / (k! * (n - k)!) = %.0lf", numerator / (denominatorPart1 * denominatorPart2));
	}
	else
	{
		printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem8(void)
{
	int n, i;
	double numerator = 1.0; // To be used for the calculation of (2n)!
	double denominatorPart1 = 1.0; // To be used for the calculation of (n + 1)!
	double denominatorPart2 = 1.0; // To be used for the calculation of n!

	printf("\nProblem 8. Catalan numbers.");
	printf("\n\n(1 < n < 100) Enter n = ");
	scanf("%d", &n);

	if ((1 < n) && (100 > n))
	{
		for (i = 1; i <= 2 * n; i++)
		{
			numerator *= i; // Calculation of (2n)!
			if (i <= (n + 1)) denominatorPart1 *= i; // Calculation of (n + 1)!
			if (i <= n) denominatorPart2 *= i; // Calculation of n!
		}

		printf("Catalan(n) = %.0lf", numerator / (denominatorPart1 * denominatorPart2));
	}
	else
	{
		if ((0 == n) || (1 == n)) printf("Catalan(n) = 1");
		else printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem9(void)
{
	int n, i, j;

	printf("\nProblem 8. Matrix of numbers.");
	printf("\n\n(1 <= n <= 200) Enter n = ");
	scanf("%d", &n);

	if ((1 <= n) && (200 >= n))
	{
		for (i = 0; i < n; i++)
		{
			for (j = 1; j <= n; j++)
			{
				if (1 < j) printf(" ");
				printf("%d", j + i);
			}
			if ((n - 1) > i) printf("\n");
		}
	}
	else
	{
		printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem10(void)
{

}
//========================================================================================================
void problem11(void)
{
	int n, min, max, i;

	printf("\nProblem 11. Random numbers in a given range [min, max].");
	printf("\n\nn = ");
	scanf("%d", &n);
	printf("min = ");
	scanf("%d", &min);
	printf("max = ");
	scanf("%d", &max);

	if ((1 <= n) && (min < max) && (0 <= min) && (1 <= max))
	{
		srand(time(NULL));

		for (i = 0; i < n; i++)
		{
			if (0 < i) printf(" ");
			printf("%d", (rand() % (max + 1 - min)) + min);
		}
	}
	else
	{
		printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem12(void)
{
	printf("\nProblem 12. * Randomize the numbers 1 ... N.\n");
	printf("NOT IMPLEMENTED.");
}
//========================================================================================================
void problem13(void)
{
	int i = 0, binaryLength = 0, bitValue, inputError = 0;
	char binary[500];
	long decimal = 0;

	printf("\nProblem 13. Binary to decimal number.");
	printf("\n\nbinary number = ");
	scanf("%s", &binary);

	while (('\0' != binary[binaryLength++]) && (500 > binaryLength)){}
	binaryLength--;

	for (i = binaryLength - 1; i >= 0; i--)
	{
		if ('0' == binary[i]) bitValue = 0;
		else if ('1' == binary[i]) bitValue = 1;
		else
		{
			inputError = 1;
			break;
		}
		decimal += (long)(bitValue * pow(2.0, (binaryLength - 1 - i)));
	}

	if (0 == inputError)
	{
		printf("decimal number = %ld", decimal);
	}
	else
	{
		printf("\nIncorrect input.");
	}
}
//========================================================================================================
void problem14(void)
{
	int i, binaryLength = 0;
	long decimal, result;
	char *binary;

	printf("\nProblem 14. Decimal to binary number.");
	printf("\n\ndecimal number = ");
	scanf("%ld", &decimal);

	result = decimal;
	do
	{
		result /= 2;
		binaryLength++;
	} while(0 < result);
	printf("\nbinaryLength = %d", binaryLength);
	binary = (char*)calloc(binaryLength + 1, sizeof(char));

	binary[binaryLength] = '\0';
	result = decimal;
	for (i = binaryLength - 1; i <= 0 ; i--)
	{
		binary[i] = result % 2;
		result /= 2;
	}
	
	printf("\nbinary number = %s", binary);

}
//========================================================================================================
void problem15(void)
{

}
//========================================================================================================
void problem16(void)
{

}
//========================================================================================================
void problem17(void)
{
	printf("\nProblem 17. * Calculate GCD.\n");
	printf("NOT IMPLEMENTED.");
}
//========================================================================================================
void problem18(void)
{
	printf("\nProblem 18. * Trailing zeroes in n!.\n");
	printf("NOT IMPLEMENTED.");
}
//========================================================================================================
void problem19(void)
{
	printf("\nProblem 19. ** Spiral matrix.\n");
	printf("NOT IMPLEMENTED.");
}
//========================================================================================================

