#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>


void main()
{
//==========================================================================================

//==========================================================================================
// ctype.h
char c = '1';
int result;

result = isdigit(c); // 1 if c is a digit (0-9)
result = isalpha(c); // 1 if c is a letter (a-z, A-Z)

result = islower('x'); // 1
c = toupper('x'); // c = 'X'
//==========================================================================================
// stdlib.h
char text[] = "1xyz";
char *remainder;
long number = strtol(text, &remainder, 10);
printf("%ld\n", number); // 1
printf("%s\n", remainder); // "xyz"
printf("%d\n", strlen(remainder)); // 3

char text[] = "1110";
char *remainder;
long dec = strtol(text, &remainder, 10); // 1100
long hex = strtol(text, &remainder, 16); // 4368
long binary = strtol(text, &remainder, 2); // 14

char *text = "-11.90003kkk";
char *remainder;
double num = strtod(text, &remainder);
//==========================================================================================
// STRCPY: strcpy(char *dest, const char *src) 

char destination[100];
strcpy(destination, "123");

char *x = strdup(destination); // => free(x);
printf("%s", x);

strncat(destination, x, 2); // "12312"

free(x);




char *text = (char*)malloc(4);
if (text)
{
	strcpy(text, "123");
	printf("%s", text);
	free(text);
}
//==========================================================================================
 // STRNCPY: char *strncpy(char *dest, const char *src, size_t n) 

char text[] = "1234";
char destination[100];

strncpy(destination, text, 3);
destination[3] = '\0';
printf("%s\n\n", destination); // "123"
//==========================================================================================
// sprintf

char destination[100];

sprintf(destination, "--->%d", 55);
printf("DESTINATION:%s\n", destination);
//==========================================================================================
// STRCMP

strcmp("A", "A"); // 0
strcmp("A", "B"); // -1
strcmp("B", "A"); // 1
//==========================================================================================
// strchr
// strrchr

char text[] = "123145";
char *x;

x = strchr(text, '1');
printf("%c", x[1]); // '2'

x = strrchr(text, '1');
printf("%c", x[1]); // '4'
//==========================================================================================
// STRSTR

char text[] = "123145";
char *x;

x = strstr(text, "31");
printf("%s", x); // "3145"
//==========================================================================================
// STRTOK

char text[] = "123- 456- 789";
char delimiter[3] = "- ";
char *token;
      

token = strtok(text, delimiter);
printf("%s\n", token);
while(1)
{
	token = strtok(NULL, delimiter);
	if (!token) break;

	printf("%s\n", token);
}

// THE ORIGINAL STRING IS MODIFIED!!!
printf("text:%s\n", text); // "123"
//==========================================================================================

//==========================================================================================
/* memset example */
#include <stdio.h>
#include <string.h>

int main ()
{
  char str[] = "almost every programmer should know memset!";
  memset (str,'-',6);
  puts (str);
  return 0;
}

Output:
------ every programmer should know memset!

	

In short:

    strcmp compares null-terminated C strings
    strncmp compares at most N characters of null-terminated C strings
    memcmp compares binary byte buffers of N bytes

So, if you have these strings:

const char s1[] = "atoms\0\0\0\0";  // extra null bytes at end
const char s2[] = "atoms\0abc";     // embedded null byte
const char s3[] = "atomsaaa";

Then these results hold true:

strcmp(s1, s2) == 0      // strcmp stops at null terminator
strcmp(s1, s3) != 0      // Strings are different
strncmp(s1, s3, 5) == 0  // First 5 characters of strings are the same
memcmp(s1, s3, 5) == 0   // First 5 bytes are the same
strncmp(s1, s2, 8) == 0  // Strings are the same up through the null terminator
memcmp(s1, s2, 8) != 0   // First 8 bytes are different


#include <stdio.h>
#include <string.h>

int main ()
{
   const char src[50] = "http://www.tutorialspoint.com";
   char dest[50];

   printf("Before memcpy dest = %s\n", dest);
   memcpy(dest, src, strlen(src)+1);
   printf("After memcpy dest = %s\n", dest);
   
   return(0);
}

Let us compile and run the above program that will produce the following result:

Before memcpy dest =
After memcpy dest = http://www.tutorialspoint.com



#include <stdio.h>
#include <string.h>

int main ()
{
   char dest[] = "oldstring";
   const char src[]  = "newstring";

   printf("Before memmove dest = %s, src = %s\n", dest, src);
   memmove(dest, src, 9);
   printf("After memmove dest = %s, src = %s\n", dest, src);

   return(0);
}

Let us compile and run the above program that will produce the following result:

Before memmove dest = oldstring, src = newstring
After memmove dest = newstring, src = newstring

//==========================================================================================

//==========================================================================================
int x = 0;
// FLUSH stdin
while ((EOF != x) && ('\n' != x))
{
	x = getchar();
}
//==========================================================================================

//==========================================================================================

//==========================================================================================

//==========================================================================================

//==========================================================================================
size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) 

fread(text, sizeof(char), 100, fromThisFile);
//==========================================================================================
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) writes data from the array pointed to, by ptr to the given stream.

#include<stdio.h>

int main ()
{
   FILE *fp;
   char str[] = "This is tutorialspoint.com";

   fp = fopen( "file.txt" , "w" );
   fwrite(str , 1 , sizeof(str) , fp );

   fclose(fp);
  
   return(0);
}
//==========================================================================================

//==========================================================================================
#include <stdio.h>
#include <string.h>

int main()
{
   FILE *fp;
   char c[] = "this is tutorialspoint";
   char buffer[100];

   /* Open file for both reading and writing */
   fp = fopen("file.txt", "w+");

   /* Write data to the file */
   fwrite(c, strlen(c) + 1, 1, fp);

   /* Seek to the beginning of the file */
   fseek(fp, SEEK_SET, 0);

   /* Read and display data */
   fread(buffer, strlen(c)+1, 1, fp);
   printf("%s\n", buffer);
   fclose(fp);
   
   return(0);
}







#include <stdio.h>

int main ()
{
   FILE *fp;

   fp = fopen("file.txt","w+");
   fputs("This is tutorialspoint.com", fp);
  
   fseek( fp, 7, SEEK_SET );
   fputs(" C Programming Language", fp);
   fclose(fp);
   
   return(0);
}
// => This is C Programming Language




#include <stdio.h>

int main ()
{
   FILE *fp;
   int c;

   fp = fopen("file.txt","r");
   while(1)
   {
      c = fgetc(fp);
      if( feof(fp) )
      {
         break;
      }
      printf("%c", c); 
   }
   fclose(fp);
   return(0);
}
//==========================================================================================
//==========================================================================================
//==========================================================================================
//==========================================================================================
//==========================================================================================
//==========================================================================================
/*
char *read_line()
{
	size_t length = 4;
	char ch;
	char *resized;
	size_t i = 0;

    char *line = (char*)malloc(4);
    if (!line)
        return NULL;
  
    
    ch = getchar();
    while (ch != '\n' && ch != EOF)
    {
        if (i == length - 1)
        {
            resized = (char*)realloc(line, 2 * length);
            if (!resized)
            {
                // Return everything we've read until resize
                line[i] = '\0';
                return line;
            }
            
            line = resized;
            length *= 2;
        }
        
        line[i] = ch;
        i++;
        
        ch = getchar();
    }
    
    line[i] = '\0';
    return line;
}



#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BUFFER_SIZE 4096

void kill(const char *);

void kill(const char *msg)
{
    if (errno)
        perror(msg);
    else 
        fprintf(stderr, "ERROR: %s", msg);
    
    exit(1);
}

int main(int argc, char **argv) 
{
    if (argc < 3)
        kill("Usage: <src-file> <dest-file>");
    
    const char *srcFileName = argv[1];
    const char *destFileName = argv[2];
    
    FILE *srcFile = fopen(srcFileName, "r");
    if (!srcFile)
        kill(srcFileName);
    
    FILE *destFile = fopen(destFileName, "w");
    if (!destFile)
        kill(destFileName);
    
    char buffer[BUFFER_SIZE];
    while (!feof(srcFile))
    {
        int readBytes = fread(buffer, 1, BUFFER_SIZE, srcFile);
        
        if (ferror(srcFile))
            kill("Error reading from source");
        
        size_t firstPartSize = readBytes / 2;
        size_t secondPartSize = readBytes - firstPartSize;
        size_t diff = secondPartSize - firstPartSize;
        
        char temp[firstPartSize];
        memcpy(temp, buffer, firstPartSize);
        memmove(buffer, buffer + firstPartSize, secondPartSize);
        memcpy(buffer + firstPartSize + diff, temp, firstPartSize);
        
        fwrite(buffer, 1, readBytes, destFile);
    }
    
    fclose(srcFile);
    fclose(destFile);
    
    return (EXIT_SUCCESS);
}


*/
//==========================================================================================
//==========================================================================================
//==========================================================================================
//==========================================================================================
}