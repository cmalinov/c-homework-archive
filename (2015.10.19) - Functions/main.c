#include<stdio.h>
#include<string.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C Functions\n\n");
	printf("Enter a number between 1 and 7 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem1(); break;
		case 2: problem2(); break;
		case 3: problem3(); break;
		case 4: problem4();	break;
		case 5: problem5(); break;
		case 6: problem6(); break;
		case 7: problem7(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem1(void)
{
	int x, y;

	printf("\nProblem 1. Bigger number.");
	printf("\n\nEnter the first integer value: ");
	scanf("%d", &x);
	printf("Enter the second integer value: ");
	scanf("%d", &y);

	printf("The bigger number is: %d", get_max(x, y));
}

int get_max(int x, int y)
{
	if (x > y) return x;
	else return y;
}
//========================================================================================================
void problem2(void)
{
	int x;

	printf("\nProblem 2. Last digit of a number.");
	printf("\n\nEnter an integer number: ");
	scanf("%d", &x);

	printf("The last digit is: ");
	print_last_digit(x);
}

void print_last_digit(int x)
{
	switch (x % 10)
	{
		case 0: printf("zero"); break;
		case 1: printf("one"); break;
		case 2: printf("two"); break;
		case 3: printf("three"); break;
		case 4: printf("four"); break;
		case 5: printf("five"); break;
		case 6: printf("six"); break;
		case 7: printf("seven"); break;
		case 8: printf("eight"); break;
		case 9: printf("nine"); break;
	}
}
//========================================================================================================
void problem3(void)
{
	char str[500], ch;

	printf("\nProblem 3. Last occurrence of a character in a string.");
	printf("\n\nEnter the string: ");
	fflush(stdin);
	gets(str);
	printf("Enter the character: ");
	fflush(stdin);
	scanf("%c", &ch);

	printf("The position of the rightmost occurrence of the character in the string is: %d", rightmost_position(ch, str));
}

int rightmost_position(char ch, char *str)
{
	int i, length = 0, position = 0;
	length = strlen(str);
	
	for (i = 0; i < length; i++)
	{
		if (str[i] == ch) position = i + 1;
	}

	if (!position) return -1;
	else return position;
}
//========================================================================================================
void problem4(void)
{
	char number[100];
	int error = 0;

	printf("\nProblem 4. Reverse number.\n\n");
	printf("Enter the floating point number: ");
	fflush(stdin);
	gets(number);

	reverse(number, &error);

	if (0 == error) printf("The reversed number is: %s", number);
	else printf("Ivalid format");
}

void reverse(char *n, int *err)
{
	int i, length = 0, pointFound = 0;
	char temp;

	length = strlen(n);

	// Validity check.
	for (i = 0; i < length; i++)
	{
		if ('.' == n[i])
		{
			pointFound++;
			if ((0 == i) || (length - 1 == i) || (1 < pointFound))
			{
				*err = 1;
				return;
			}
		}
		else if (('0' != n[i]) && ('1' != n[i]) && ('2' != n[i]) && ('3' != n[i]) && ('4' != n[i]) &&
			     ('5' != n[i]) && ('6' != n[i]) && ('7' != n[i]) && ('8' != n[i]) && ('9' != n[i]))
		{
			*err = 1;
			return;
		}
	}

	// Reversing the number.
	for (i = 0; i < length / 2; i++)
	{
		temp = n[i];
		n[i] = n[length - 1 - i];
		n[length - 1 - i] = temp;
	}
}
//========================================================================================================
void problem5(void)
{
	int arr[] = {1, 3, 4, 5, 1, 0, 5};
	int arrLength;
	int i, elementToFind;

	printf("\nProblem 5. Aray manipulation.\n\n");

	arrLength = sizeof(arr)/sizeof(arr[0]);

	printf("The input array is:");
	for (i = 0; i < arrLength; i++)
	{
		printf(" %d", arr[i]);
	}

	// NOTE: The array manipulation functions used below can be found in the file array_manipulation.c.

	// MIN element
	printf("\nThe smallest element is: %d", arr_min(arr, arrLength));

	// MAX element
	printf("\nThe largest element is: %d", arr_max(arr, arrLength));
	
	// AVERAGE
	printf("\nThe average of all elements is: %f", arr_average(arr, arrLength));
	
	// SUM
	printf("\nThe sum of all elements is: %d", arr_sum(arr, arrLength));
	
	// FIND element
	printf("\nEnter a value to be searched in the array: ");
	scanf("%d", &elementToFind);
	if (arr_contains(arr, arrLength, elementToFind)) printf("The array contains an element with value %d.", elementToFind);
	else printf("The array does not contain an element with value %d.", elementToFind);

	// MERGE *
	// To be implemented

	// CLEAR array
	printf("\nThe cleared array is:");
	arr_clear(arr, arrLength);
	for (i = 0; i < arrLength; i++)
	{
		printf(" %d", arr[i]);
	}
}
//========================================================================================================
void problem6(void)
{
	int arr[] = {1, 3, 4, 5, 1, 0, 5};
	//int arr[] = {1, 2, 3, 4, 5, 6, 6};
	//int arr[] = {1, 1, 1};
	int i, arrLength;

	printf("\nProblem 6. First larger than its neighbours.\n\n");

	arrLength = sizeof(arr)/sizeof(arr[0]);

	printf("The input array is:");
	for (i = 0; i < arrLength; i++)
	{
		printf(" %d", arr[i]);
	}

	printf("\nThe index of the first element larger than its neighbours is: %d", first_larger(arr, arrLength));
}

int first_larger(int *arrayX, int arrayLength)
{
	int i;

	for (i = 1; i < arrayLength - 1; i++)
	{
		if ((arrayX[i-1] < arrayX[i]) && (arrayX[i] > arrayX[i + 1])) return i;
	}

	return -1;
}
//========================================================================================================
void problem7(void)
{
	char inputString[500];
	char outputString[500];
	int stringLength;

	printf("\nProblem 7. Reversing a string recursively.\n\n");
	printf("Enter a string to be reversed: ");
	fflush(stdin);
	gets(inputString);

	stringLength = strlen(inputString);
	mirror_string(inputString, stringLength, outputString);
	outputString[stringLength] = '\0';

	printf("The reversed string is: %s", outputString);
}

void mirror_string(char *inputString, int stringLength, char *outputString)
{
	if ('\0' != *inputString) // VARIANT 1
	//if (0 < stringLength) // VARIANT 2
	{
		*(outputString + stringLength - 1) = *inputString;

		mirror_string(inputString + 1, stringLength - 1, outputString);
	}
}
//========================================================================================================
