

int get_max(int x, int y);
void print_last_digit(int x);
int rightmost_position(char ch, char *str);
void reverse(char *n, int *err);
int arr_min(int *arrayX, int arrayLength);
int arr_max(int *arrayX, int arrayLength);
float arr_average(int *arrayX, int arrayLength);
int arr_sum(int *arrayX, int arrayLength);
int arr_contains(int *arrayX, int arrayLength, int element);
int* arr_merge(int *arrayX, int arrayXLength, int *arrayY, int arrayYLength);
void arr_clear(int *arrayX, int arrayLength);
int first_larger(int *arrayX, int arrayLength);
void mirror_string(char *inputString, int stringLength, char *outputString);

void problem1(void);
void problem2(void);
void problem3(void);
void problem4(void);
void problem5(void);
void problem6(void);
void problem7(void);

