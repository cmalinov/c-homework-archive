#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C arrays\n\n");
	printf("Enter a number between 1 and 17 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem_1(); break;
		case 2: problem_2(); break;
		case 3: problem_3(); break;
		case 4: problem_4(); break;
		case 5: problem_5(); break;
		case 6: problem_6(); break;
		case 7: problem_7(); break;
		case 8: problem_8(); break;
		case 9: problem_9(); break;
		case 10: problem_10(); break;
		case 11: problem_11(); break;
		case 12: problem_12(); break;
		case 13: problem_13(); break;
		case 14: problem_14(); break;
		case 15: problem_15(); break;
		case 16: problem_16(); break;
		case 17: problem_17(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	int n, i;
	int *inputArray;

	printf("\nProblem 1. Save and print numbers in range.\n\n");

	fflush(stdin);
	printf("n = ");
	scanf("%d", &n);

	inputArray = (int*)calloc(n, sizeof(int));

	for (i = 0; i < n; i++) scanf("%d", &inputArray[i]);

	print_int_array(inputArray, n);


	free(inputArray);
}

void print_int_array(int *arrayX, int arrayLength)
{
	int i;

	for (i = 0; i < arrayLength; i++)
	{
		if (0 < i) printf(" ");
		printf("%d", arrayX[i]);
	}
}
//========================================================================================================
void problem_2(void)
{
	int n, i;
	int *inputArray;
	int numberToSearch;

	printf("\nProblem 2. Linear search.\n\n");

	fflush(stdin);
	scanf("%d", &n);

	inputArray = (int*)calloc(n, sizeof(int));

	for (i = 0; i < n; i++) scanf("%d", &inputArray[i]);
	scanf("%d", &numberToSearch);

	array_contains_element(inputArray, n, numberToSearch);


	free(inputArray);
}

void array_contains_element(int *arrayX, int arrayLength, int elementToFind)
{
	int i;

	for (i = 0; i < arrayLength; i++)
	{
		if (elementToFind == arrayX[i])
		{
			printf("yes");
			return;
		}
	}
	printf("no");
}
//========================================================================================================
void problem_3(void)
{
	int length, i;
	int *inputArray;

	printf("\nProblem 3. Sort array of numbers.\n\n");

	fflush(stdin);
	scanf("%d", &length);

	inputArray = (int*)calloc(length, sizeof(int));

	for (i = 0; i < length; i++) scanf("%d", &inputArray[i]);

	sort_int_array(inputArray, length);
	print_int_array(inputArray, length); // The function is defined in the section of Problem 1 above.


	free(inputArray);
}

void sort_int_array(int *arrayX, int arrayLength)
{
	int i, j, temp;

	for (i = 0; i < arrayLength - 1; i++)
	{
		for (j = 0; j < arrayLength - 1; j++)
		{
			if (arrayX[j] > arrayX[j + 1])
			{
				temp = arrayX[j];
				arrayX[j] = arrayX[j + 1];
				arrayX[j + 1] = temp;
			}
		}	
	}
}
//========================================================================================================
void problem_4(void)
{
	int length, i, r = 0, f = 0;
	float *inputArray, *roundNumbers, *floatingPointNumbers;

	printf("\nProblem 4. Categorize numbers and find Min, Max and Average.\n\n");

	fflush(stdin);
	scanf("%d", &length);

	inputArray = (float*)calloc(length, sizeof(float));
	roundNumbers = (float*)calloc(length, sizeof(float));
	floatingPointNumbers = (float*)calloc(length, sizeof(float));

	for (i = 0; i < length; i++)
	{
		scanf("%f", &inputArray[i]);

		if (inputArray[i] == (int)inputArray[i])
		{
			roundNumbers[r++] = inputArray[i];
		}
		else
		{
			floatingPointNumbers[f++] = inputArray[i];
		}
	}

	if (0 < f)
	{
		printf("[");
		print_float_array(floatingPointNumbers, f);
		printf("] -> min: %.2f, ", arr_float_min(floatingPointNumbers, f));
		printf("max: %.2f, ", arr_float_max(floatingPointNumbers, f));
		printf("sum: %.2f, ", arr_float_sum(floatingPointNumbers, f));
		printf("avg: %.2f", arr_float_average(floatingPointNumbers, f));
	}

	if (0 < r)
	{
		printf("\n[");
		print_float_array(roundNumbers, r);
		printf("] -> min: %.0f, ", arr_float_min(roundNumbers, r));
		printf("max: %.0f, ", arr_float_max(roundNumbers, r));
		printf("sum: %.0f, ", arr_float_sum(roundNumbers, r));
		printf("avg: %.0f", arr_float_average(roundNumbers, r));
	}


	free(inputArray);
	free(roundNumbers);
	free(floatingPointNumbers);
}

void print_float_array(float *arrayX, int arrayLength)
{
	int i;

	for (i = 0; i < arrayLength; i++)
	{
		if (0 < i) printf(" ");
		printf("%.2f", arrayX[i]);
	}
}

float arr_float_min(float *arrayX, int arrayLength)
{
	int i;
	float min;

	min = arrayX[0];
	for (i = 0; i < arrayLength; i++)
	{
		if (min > arrayX[i]) min = arrayX[i];
	}

	return min;
}

float arr_float_max(float *arrayX, int arrayLength)
{
	int i;
	float max;

	max = arrayX[0];
	for (i = 0; i < arrayLength; i++)
	{
		if (max < arrayX[i]) max = arrayX[i];
	}

	return max;
}

float arr_float_sum(float *arrayX, int arrayLength)
{
	int i;
	float sum = 0.0;

	for (i = 0; i < arrayLength; i++)
	{
		sum += arrayX[i];
	}

	return sum;
}

float arr_float_average(float *arrayX, int arrayLength)
{
	int i;
	float sum = 0.0;

	for (i = 0; i < arrayLength; i++)
	{
		sum += arrayX[i];
	}

	return sum/arrayLength;
}
//========================================================================================================
void problem_5(void)
{
	int length, i, j;
	int *inputArray;
	int *sequenceBuffer;
	int sequenceBufferLength = 0;
	int longestSequenceLength = 0, longestSequenceIndex = 0;
	int sequencesNumber = 0, *sequencesLengths;
	int **sequencesMatrix;

	printf("\nProblem 5. Longest increasing sequence.\n\n");

	fflush(stdin);
	scanf("%d", &length);

	inputArray = (int*)calloc(length, sizeof(int));
	sequenceBuffer = (int*)calloc(length, sizeof(int));
	sequencesLengths = (int*)calloc(length, sizeof(int));
	sequencesMatrix = (int**)calloc(length, sizeof(int*));

	// When a number is entered, it is distributed in the matrix holding all the sequences
	for (i = 0; i < length; i++)
	{
		scanf("%d", &inputArray[i]);

		if (0 == i) // PROCESSING OF THE FIRST ELEMENT
		{
			sequenceBuffer[sequenceBufferLength++] = inputArray[i];
		}
		else
		{
			if (inputArray[i - 1] < inputArray[i])
			{
				// The current element is added to the buffer holding the last increasing sequence.
				sequenceBuffer[sequenceBufferLength++] = inputArray[i];
			}
			else // The current element is the first element of a new increasing sequence.
			{
				// The buffer is saved in the matrix.
				sequencesMatrix[sequencesNumber] = (float*)calloc(sequenceBufferLength, sizeof(float));
				for (j = 0; j < sequenceBufferLength; j++)
				{
					sequencesMatrix[sequencesNumber][j] = sequenceBuffer[j];
				}

				// The length of the last completed sequence is saved in the reference array.
				sequencesLengths[sequencesNumber] = sequenceBufferLength;

				// It is checked whether the last completed sequence is the logest one.
				if (sequenceBufferLength > longestSequenceLength)
				{
					longestSequenceLength = sequenceBufferLength;
					longestSequenceIndex = sequencesNumber;
				}

				// A completion of a sequence is detected, so the number of detected sequences is increased by 1.
				sequencesNumber++;

				// The buffer's counter is reset and the current element is placed in the buffer.
				sequenceBufferLength = 0;
				sequenceBuffer[sequenceBufferLength++] = inputArray[i];
			}

			if ((length - 1) == i) // PROCESSING OF THE LAST ELEMENT
			{
				// The current element is the last element of the last sequence.
				// The buffer is saved in the matrix.
				sequencesMatrix[sequencesNumber] = (int*)calloc(sequenceBufferLength, sizeof(int));
				for (j = 0; j < sequenceBufferLength; j++)
				{
					sequencesMatrix[sequencesNumber][j] = sequenceBuffer[j];
				}

				// It is checked whether the last completed sequence is the logest one.
				if (sequenceBufferLength > longestSequenceLength)
				{
					longestSequenceLength = sequenceBufferLength;
					longestSequenceIndex = sequencesNumber;
				}

				// The length of the last completed sequence is saved in the reference array.
				sequencesLengths[sequencesNumber] = sequenceBufferLength;

				// A completion of a sequence is detected, so the number of detected sequences is increased by 1.
				sequencesNumber++;
			}
		}
	}

	// OUTPUT
	for (i = 0; i < sequencesNumber; i++)
	{
		for (j = 0; j < sequencesLengths[i]; j++)
		{		
			if (0 < j) printf(" ");
			printf("%d", sequencesMatrix[i][j]);
		}
		printf("\n");
	}
	printf("Longest:");
	for (j = 0; j < longestSequenceLength; j++)
	{
		printf(" %d", sequencesMatrix[longestSequenceIndex][j]);
	}


	free(inputArray);
	free(sequenceBuffer);
	free(sequencesLengths);
	free(sequencesMatrix);
	for (i = 0; i < sequencesNumber; i++)
	{
		free(sequencesMatrix[i]);
	}
}
//========================================================================================================
void problem_6(void)
{
	int i, j;
	int *listOne, listOneLength, *listTwo, listTwoLength;
	int *listBoth;

	printf("\nProblem 6. Join lists.\n\n");

	fflush(stdin);
	scanf("%d", &listOneLength);
	scanf("%d", &listTwoLength);

	listOne = (int*)calloc(listOneLength, sizeof(int));
	listTwo = (int*)calloc(listTwoLength, sizeof(int));
	listBoth = (int*)calloc(listOneLength + listTwoLength, sizeof(int));

	for (i = 0; i < listOneLength; i++)
	{
		scanf("%d", &listOne[i]);
	}
	for (i = 0; i < listTwoLength; i++)
	{
		scanf("%d", &listTwo[i]);
	}

	// JOINING THE TWO LISTS
	for (i = 0; i < listOneLength + listTwoLength; i++)
	{
		if (listOneLength > i)
		{
			listBoth[i] = listOne[i];
		}
		else
		{
			listBoth[i] = listTwo[i - listOneLength];
		}
	}

	// SORTING THE BIG LIST
	sort_int_array(listBoth, listOneLength + listTwoLength); // The function is defined in the section of Problem 3 above.

	// OUTPUT
	printf("%d", listBoth[0]);
	for (i = 1; i < listOneLength + listTwoLength; i++)
	{
		if (listBoth[i - 1] != listBoth[i]) printf(" %d", listBoth[i]);
	}

	free(listOne);
	free(listTwo);
	free(listBoth);
}
//========================================================================================================
void problem_7(void)
{
	int i;
	int *inputArray, length;

	printf("\nProblem 7. Reverse array.\n\n");

	fflush(stdin);
	scanf("%d", &length);
	inputArray = (int*)calloc(length, sizeof(int));
	for (i = 0; i < length; i++) scanf("%d", &inputArray[i]);

	reverse_int_array(inputArray, length);
	print_int_array(inputArray, length); // The function is defined in the section of Problem 1 above.


	free(inputArray);
}

void reverse_int_array(int *inputArray, int length)
{
	int i, temp;

	for (i = 0; i < length / 2; i++)
	{
		temp = inputArray[i];
		inputArray[i] = inputArray[length - 1 - i];
		inputArray[length - 1 - i] = temp;
	}
}
//========================================================================================================
void problem_8(void)
{
	int i, currentIndex = 0, start, end, directionToGo = 0;
	int length, *inputArray, numberToFind;

	printf("\nProblem 8. Iterative binary search.\n\n");

	fflush(stdin);
	scanf("%d", &length);
	inputArray = (int*)calloc(length, sizeof(int));
	for (i = 0; i < length; i++) scanf("%d", &inputArray[i]);
	scanf("%d", &numberToFind);

	start = 0;
	end = length;
	directionToGo = 0;
	do
	{
		if (1 == directionToGo) end = currentIndex;
		else if (2 == directionToGo) start = currentIndex;

		currentIndex = start + (end - start) / 2;

		if (numberToFind == inputArray[currentIndex])
		{
			printf("%d", currentIndex);
			free(inputArray);
			return;
		}
		else if (numberToFind < inputArray[currentIndex])
		{
			directionToGo = 1; // Search to the left.
		}
		else
		{
			directionToGo = 2; // Search to the right.
		}
	} while (end != start);


	printf("-1");
	free(inputArray);
}
//========================================================================================================
void problem_9(void)
{
	
}
//========================================================================================================
void problem_10(void)
{
	int i, row, column;
	int size, **inputMatrix;

	printf("\nProblem 10. Numbers beneath the main diagonal.\n\n");

	fflush(stdin);
	scanf("%d", &size);
	
	inputMatrix = (int**)calloc(size, sizeof(int*));
	for (i = 0; i < size; i++)
	{
		inputMatrix[i] = (int*)calloc(size, sizeof(int));
	}

	for (row = 0; row < size; row++)
	{
		for (column = 0; column < size; column++)
		{
			scanf("%d", &inputMatrix[row][column]);
		}
	}

	// OUTPUT
	for (row = 0; row < size; row++)
	{
		for (column = 0; column < row + 1; column++)
		{
			if (0 < column) printf(" ");
			printf("%d", inputMatrix[row][column]);
		}
		if ((size - 1) > row) printf("\n");
	}
	


	for (i = 0; i < size; i++)
	{
		free(inputMatrix[i]);
	}
	free(inputMatrix);
}
//========================================================================================================
void problem_11(void)
{
	int i, size, *vector, scalar;

	printf("\nProblem 11. Scalar multiplication of a vector.\n\n");

	fflush(stdin);
	scanf("%d", &size);
	scanf("%d", &scalar);
	vector = (int*)calloc(size, sizeof(int));
	for (i = 0; i < size; i++) scanf("%d", &vector[i]);

	array_multiplication(vector, size, scalar);
	print_int_array(vector, size); // The function is defined in the section of Problem 1 above.


	free(vector);
}

void array_multiplication(int *arrayX, int arrayLength, int multiplier)
{
	int i;

	for (i = 0; i < arrayLength; i++)
	{
		arrayX[i] *= multiplier;
	}
}
//========================================================================================================
void problem_12(void)
{
	int i, size, sum = 0;
	int *inputArray; // Both vectors are hold in a single array.

	printf("\nProblem 12. Dot product of vectors.\n\n");

	fflush(stdin);
	scanf("%d", &size);
	inputArray = (int*)calloc(size * 2, sizeof(int));
	for (i = 0; i < size * 2; i++) scanf("%d", &inputArray[i]);

	for (i = 0; i < size; i++)
	{
		sum += inputArray[i] * inputArray[i + size];
	}
	printf("%d", sum);


	free(inputArray);
}
//========================================================================================================
void problem_13(void)
{
	
}
//========================================================================================================
void problem_14(void)
{
	int i, j, rows, columns;
	int **inputArray; // Both matrices are hold in a single two-dimensional array.

	printf("\nProblem 14. Sum of matrices.\n\n");

	fflush(stdin);
	scanf("%d", &rows);
	scanf("%d", &columns);
	
	inputArray = (int**)calloc(rows * 2, sizeof(int*));
	for (i = 0; i < rows * 2; i++)
	{
		inputArray[i] = (int*)calloc(columns, sizeof(int));

		for (j = 0; j < columns; j++)
		{
			scanf("%d", &inputArray[i][j]);
		}
	}


	// OUTPUT
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < columns; j++)
		{
			if (0 < j) printf(" ");
			printf("%d", inputArray[i][j] + inputArray[i + rows][j]);
		}
		if ((rows - 1) > i) printf("\n");
	}

	

	for (i = 0; i < rows; i++)
	{
		free(inputArray[i]);
	}
	free(inputArray);
}
//========================================================================================================
void problem_15(void)
{
	int i, j, k, currentElement;
	int **matrixOne, matrixOneRows, matrixOneColumns;
	int **matrixTwo;

	printf("\nProblem 15. Multiplication of matrices.\n\n");

	// DIMENSIONS
	fflush(stdin);
	scanf("%d", &matrixOneRows);
	scanf("%d", &matrixOneColumns);
	
	// MATRIX ONE
	matrixOne = (int**)calloc(matrixOneRows, sizeof(int*));
	for (i = 0; i < matrixOneRows; i++)
	{
		matrixOne[i] = (int*)calloc(matrixOneColumns, sizeof(int));

		for (j = 0; j < matrixOneColumns; j++)
		{
			scanf("%d", &matrixOne[i][j]);
		}
	}

	// MATRIX TWO
	matrixTwo = (int**)calloc(matrixOneColumns, sizeof(int*));
	for (i = 0; i < matrixOneColumns; i++)
	{
		matrixTwo[i] = (int*)calloc(matrixOneRows, sizeof(int));

		for (j = 0; j < matrixOneRows; j++)
		{
			scanf("%d", &matrixTwo[i][j]);
		}
	}


	// OUTPUT
	for (i = 0; i < matrixOneRows; i++)
	{
		for (j = 0; j < matrixOneRows; j++)
		{
			currentElement = 0;
			for (k = 0; k < matrixOneColumns; k++)
			{
				currentElement += matrixOne[i][k] * matrixTwo[k][j];
			}

			if (0 < j) printf(" ");
			printf("%d", currentElement);
		}
		if ((matrixOneRows - 1) > i) printf("\n");
	}


	
	// FREE MEMORY
	//
	for (i = 0; i < matrixOneRows; i++)
	{
		free(matrixOne[i]);
	}
	free(matrixOne);
	//
	for (i = 0; i < matrixOneColumns; i++)
	{
		free(matrixTwo[i]);
	}
	free(matrixTwo);
}
//========================================================================================================
void problem_16(void)
{
	int i, j, k;
	int lineLength, rows;
	char text[1001];
	char **matrix;

	printf("\nProblem 16. Text gravity.\n\n");

	fflush(stdin);
	scanf("%d", &lineLength);
	fflush(stdin);
	gets(text);

	// DETERMINATION OF THE NUMBER OF ROWS
	if (0 == (strlen(text) % lineLength)) rows = strlen(text) / lineLength;
	else rows = (strlen(text) / lineLength) + 1;

	// CONSTRUCTION OF THE MATRIX
	matrix = (char**)calloc(rows, sizeof(char*));
	for (i = 0; i < rows; i++)
	{
		matrix[i] = (char*)calloc(lineLength, sizeof(char));
	}
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < lineLength; j++)
		{
			matrix[i][j] = '*';
		}
	}
	i = 0;
	j = 0;
	for (k = 0; k < strlen(text); k++)
	{
		if ((k / (i + 1)) == lineLength)
		{
			i++;
			j = 0;
			matrix[i][j] = text[k];
		}
		else
		{
			matrix[i][j++] = text[k];
		}
	}

	
	// GRAVITY
	for (i = rows - 1; i > 0; i--)
	{
		for (j = 0; j < lineLength; j++)
		{
			if ('*' == matrix[i][j])
			{
				matrix[i][j] = matrix[i-1][j];
				matrix[i-1][j] = '*';
			}
		}
	}
	
	

	// OUTPUT
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < lineLength; j++)
		{
			//if (0 < j) printf(" ");
			printf("%c", matrix[i][j]);
		}
		if ((rows - 1) > i) printf("\n");
	}


	for (i = 0; i < rows; i++)
	{
		free(matrix[i]);
	}
	free(matrix);
}
//========================================================================================================
void problem_17(void)
{

}
//========================================================================================================