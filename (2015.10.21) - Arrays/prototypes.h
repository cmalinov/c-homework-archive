

void print_int_array(int *arrayX, int arrayLength);
void array_contains_element(int *arrayX, int arrayLength, int elementToFind);
void sort_int_array(int *arrayX, int arrayLength);
void print_float_array(float *arrayX, int arrayLength);
float arr_float_min(float *arrayX, int arrayLength);
float arr_float_max(float *arrayX, int arrayLength);
float arr_float_sum(float *arrayX, int arrayLength);
float arr_float_average(float *arrayX, int arrayLength);
void reverse_int_array(int *inputArray, int length);
void array_multiplication(int *arrayX, int arrayLength, int multiplier);

void problem_1(void);
void problem_2(void);
void problem_3(void);
void problem_4(void);
void problem_5(void);
void problem_6(void);
void problem_7(void);
void problem_8(void);
void problem_9(void);
void problem_10(void);
void problem_11(void);
void problem_12(void);
void problem_13(void);
void problem_14(void);
void problem_15(void);
void problem_16(void);
void problem_17(void);



