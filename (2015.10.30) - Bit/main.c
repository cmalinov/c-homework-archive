#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C bit manipulation\n\n");
	printf("Enter a number between 1 and 9 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem_1(); break;
		case 2: problem_2(); break;
		case 3: problem_3(); break;
		case 4: problem_4(); break;
		case 5: problem_5(); break;
		case 6: problem_6(); break;
		case 7: problem_7(); break;
		case 8: problem_8(); break;
		case 9: problem_9(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	unsigned int n = 0;

	printf("\nProblem 1. First bit. Third bit. \n\n");

	printf("n = ");
	scanf("%d", &n);

	printf("Bit 1: %u", (n & 2) >> 1);
	printf("\nBit 3: %u", (n & 8) >> 3);
}
//========================================================================================================
void problem_2(void)
{
	int n = 0, p = 0;

	printf("\nProblem 2. Extract a bit from an integer.\n\n");

	printf("n = ");
	scanf("%d", &n);
	printf("bit position = ");
	scanf("%d", &p);

	printf("Bit at position %d: %d", p, (n & (1<<p)) >> p);
}
//========================================================================================================
void problem_3(void)
{
	int n = 0, p = 0;

	printf("\nProblem 3. Check a bit at a given position.\n\n");

	printf("n = ");
	scanf("%d", &n);
	printf("bit position = ");
	scanf("%d", &p);

	printf("Bit at position %d == 1: %s", p, ((n & (1<<p)) >> p)?"true":"false");
}
//========================================================================================================
void problem_4(void)
{
	int n = 0, p = 0, mask = 1;

	printf("\nProblem 4. Bit destroyer.\n\n");

	printf("n = ");
	scanf("%d", &n);
	printf("bit position = ");
	scanf("%d", &p);

	mask <<= p;
	n |= mask;
	n ^= mask;

	printf("%d", n);
}
//========================================================================================================
void problem_5(void)
{
	int n = 0, p = 0, v = 0;

	printf("\nProblem 5. Modify a bit at a given position.\n\n");

	printf("n = ");
	scanf("%d", &n);
	printf("p = ");
	scanf("%d", &p);
	printf("v = ");
	scanf("%d", &v);

	printf("%d", v?n|(1<<p):n&(~(1<<p)));
}
//========================================================================================================
void problem_6(void)
{
	int n = 0;

	printf("\nProblem 6. Bits exchange.\n\n");

	printf("n = ");
	scanf("%d", &n);

	exchange_bits(n, 3, 24, 3);
}

void exchange_bits(int n, int p, int q, int k)
{
	int i;
	int setP = 0; // k bits starting from bit p.
	int setQ = 0; // k bits starting from bit q.
	int mask = 0;

	// Print binary representation of n.
	print_int_binary(n);

	// REPLACEMENT
	for (i = 0; i < k; i++) mask += 1 << i;
	setP = (n & (mask << p)) >> p;
	setQ = (n & (mask << q)) >> q;
	n &= ~(mask << p);
	n &= ~(mask << q);
	n |= setP << q;
	n |= setQ << p;

	// Print binary result.
	printf("\n");
	print_int_binary(n);
	
	// Print decimal result.
	printf("\n%d", n);
}

void print_int_binary(int n)
{
	int i;

	for (i = sizeof(int) * 8 - 1 ; i >=0 ; i--)
	{
		if ((0 == ((i + 1) % 8)) && ((sizeof(int) * 8 - 1) > i)) printf(" ");
		printf("%d", (0 != (n & (1<<i)))?1:0);
	}
}
//========================================================================================================
void problem_7(void)
{
	int n = 0, p = 0, q = 0, k = 0;

	printf("\nProblem 7. Bits exchange (advanced).\n\n");

	printf("n = ");
	scanf("%d", &n);
	printf("p = ");
	scanf("%d", &p);
	printf("q = ");
	scanf("%d", &q);
	printf("k = ");
	scanf("%d", &k);

	exchange_bits(n, p, q, k); // exchange_bits is defined in the section for Problem 6 above.
}
//========================================================================================================
void problem_8(void)
{
	int n, step, i, j, k, numbers[100];
	int bin[800];
	int mask;

	printf("\nProblem 8. Bits up.\n\n");
	
	// INPUT
	scanf("%d", &n);
	scanf("%d", &step);
	for (i = 0; i < n; i++)
	{
		scanf("%d", &numbers[i]);
	}

	// INPUT BYTES INTO ARRAY
	for (i = 0; i < n; i++)
	{
		mask = 1;
		mask <<= 7;
		for (j = 0; j < 8; j++)
		{
			if (0 < j) mask >>= 1;
			if (0 != (numbers[i] & mask))
			{
				bin[(8 * i) + j] = 1;
			}
			else
			{
				bin[(8 * i) + j] = 0;
			}
		}
	}

	// SETTING BITS TO 1
	for (i = 0; i < n * 8; i++)
	{
		for (j = 0; j < n * 8; j++)
		{
			if ((1 + j * step) == i) bin[i] = 1;
		}
	}

	// ARRAY BITS TO NUMBERS
	k = -1;
	for (i = 0; i < n * 8; i++)
	{
		if ((0 == (i + 1) % 8) && (0 < i))
		{
			k++;
			numbers[k] = 0;
			for (j = 7; j >= 0; j--)
			{
				numbers[k] += bin[i - j] * (1 << j);
			}
		}
	}

	// OUTPUT
	for (i = 0; i < n; i++)
	{
		if (0 < i) printf("\n");
		printf("%d", numbers[i]);
	}
}
//========================================================================================================
void problem_9(void)
{
	unsigned long long number, sieves[100], sieve = 0, mask;
	int N;
	int i, counter = 0;

	printf("\nProblem 9. Bit sifting.\n\n");
	
	// INPUT
	scanf("%llu", &number);
	scanf("%d", &N);
	for (i = 0; i < N; i++)
	{
		scanf("%llu", &sieves[i]);
	}

	// MAKING ONLY ONE SIEVE
	for (i = 0; i < N; i++)
	{
		sieve |= sieves[i];
	}

	// SIFTING
	number |= sieve;
	number ^= sieve;

	// COUNTING THE 1s
	mask = 1;
	for (i = 0; i < sizeof(unsigned long long) * 8; i++)
	{
		if (0 != (number & mask)) counter++;
		mask <<= 1;
	}

	// OUTPUT
	printf("%d", counter);
}
//========================================================================================================



