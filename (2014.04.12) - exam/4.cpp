// 4-magic dates

#include<stdio.h>
#include<conio.h>
#include <iostream>
using namespace std;


int main(void)
{
	int startyear, endyear, weight, found = 0;
	int day, month, year, leap = 0, days;
	int magic[8], i, j, result = 0;

	cin >> startyear;
	cin >> endyear;
	cin >> weight;

	// 01 01 1900

	// 31 12 2100

	for (year = startyear; year < endyear + 1; year++)
	{
		for (month = 1; month < 13; month++)
		{
			if ((year > 1903) && (((year - 1900) % 4) == 0)) leap = 1;
			else leap = 0;

			if (1 == month) days = 31;
			if (2 == month)
			{
				if (1 == leap) days = 29;
				else days = 28;
			}
			if (3 == month) days = 31;
			if (4 == month) days = 30;
			if (5 == month) days = 31;
			if (6 == month) days = 30;
			if (7 == month) days = 31;
			if (8 == month) days = 31;
			if (9 == month) days = 30;
			if (10 == month) days = 31;
			if (11 == month) days = 30;
			if (12 == month) days = 31;

			for (day = 1; day < days + 1; day++)
			{
				// day weight
				if (day < 10) magic[0] = 0;
				else magic[0] = (day / 10);
				//
				magic[1] = day % 10;

				// month weight
				if (month < 10) magic[2] = 0;
				else magic[2] = (month / 10);
				//
				magic[3] = month % 10;

				// year wight
				if (year < 2000) magic[4] = 1;
				else magic[4] = 2;
				//
				if (year < 2000) magic[5] = 9;
				else
				{
					if (year < 2100) magic[5] = 0;
					else magic[5] = 1;
				}
				//
				magic[6] = ((year % 100) - (year % 10)) / 10;
				//
				magic[7] = year % 10;



				//for (i = 0; i < 8; i++)
				//{
					// printf("%d", magic[i]);
				//}
				//printf("\n");




				// calculation
				result = 0;
				for (i = 0; i < 8; i++)
				{
					for (j = i + 1; j < 8; j++)
					{
						result = result + magic[i] * magic[j];
					}
				}

				if (weight == result)
				{
					found++;

					// printing
					if (found > 1) printf("\n");

					//printf("%d -> ", result);

					printf("%d%d-%d%d-%d%d%d%d", magic[0], magic[1], magic[2], magic[3], magic[4], magic[5], magic[6], magic[7]);
				}
			}
		}
	}

	if (0 == found) printf("No");
}