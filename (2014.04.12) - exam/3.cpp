//3-house
#include<stdio.h>
#include<conio.h>
#include <iostream>
using namespace std;


int main(void)
{
	int i, j, N, dotend, dotmiddle;

	//scanf_s("%d", &N);
	cin >> N;


	dotend = (N - 1) / 2;
	dotmiddle = 0;

	// print roof
	for (i = 0; i < (N - 1) / 2; i++)
	{
		//print dots left
		for (j = 0; j < dotend; j++)
		{
			printf(".");
		}

		// print side 1
		printf("*");

		if (i > 0)
		{
			if ((dotmiddle > 1) && ((dotmiddle % 2) == 0)) dotmiddle++;
			//print dots middle
			for (j = 0; j < dotmiddle; j++)
			{
				printf(".");
			}

			// print side 2
			printf("*");
		}

		//print dots right
		for (j = 0; j < dotend; j++)
		{
			printf(".");
		}

		printf("\n");

		dotend--;
		dotmiddle++;
	}


	// print roof base
	for (i = 0; i < N; i++)
	{
		printf("*");
	}
	printf("\n");




	// print house
	for (j = 0; j < (((N - 1) / 2) - 1); j++)
	{
		// print left dots
		for (i = 0; i < ((N - 1) / 4); i++)
		{
			printf(".");
		}

		printf("*");

		for (i = 0; i < N - 2 - 2 * (((N - 1) / 4)); i++)
		{
			printf(".");
		}

		printf("*");

		// print right dots
		for (i = 0; i < ((N - 1) / 4); i++)
		{
			printf(".");
		}

		printf("\n");
	}

	// print house base
	//
	// print left dots
	for (i = 0; i < ((N - 1) / 4); i++)
	{
		printf(".");
	}
	//
	for (i = 0; i < (N - 2*((N - 1) / 4)); i++)
	{
		printf("*");
	}
	// print right dots
	for (i = 0; i < ((N - 1) / 4); i++)
	{
		printf(".");
	}
}