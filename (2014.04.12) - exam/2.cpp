//2-pairs
#include<stdio.h>
#include<conio.h>
#include <iostream>
using namespace std;

int char_to_int(char x);

int main(void)
{
	char input[7000];
	char number1[1000][6], number2[1000][6];
	char x;
	int i, j, k, first = 1;
	int pair, length = 0, negative = 0, number, position, digit;
	int N = 0;
	int pairone[1000], pairtwo[1000], value[1000], difference[1000], value1, value2, allsame = 1, maxdiff = 0, neg1 = 0, neg2 = 0;

	cin.getline(input, 7000);
	//cout << '\n' << input << '\n';

	//i = 0;
	//do
	//{
	// x = input[i];
	// if (x != '\0') printf("%c", x);
	// i++;
	//} while (x != '\0');


	// numbers identification
	i = 0;
	j = 0;
	pair = 0;
	do
	{
		do
		{
			x = input[i];
			if (((x > 47) && (x < 58)) || (x == '-'))
			{
				if (1 == first) number1[pair][j] = x;
				else number2[pair][j] = x;
			}

			i++;
			j++;

			if ((x == ' ') || (x == '\0'))
			{
				if (1 == first) number1[pair][j-1] = '\0';
				else number2[pair][j-1] = '\0';

				if (1 == first) first = 0;
				else
				{
					first = 1;
					pair++;
				}

				j = 0;
			}
		} while ((x != '\0') && (x != ' '));
	} while (x != '\0');

	// - 9 9 4 \0
	/*
	printf("\n");

	for (i = 0; i < pair; i++)
	{
		j = 0;
		printf("pair[%d] = ", i);
		do
		{
			x = number1[i][j];
			if (x != '\0') printf("%c", x);

			j++;
		} while (x != '\0');

		j = 0;
		printf("+");
	}

	printf("\n");

	for (i = 0; i < pair; i++)
	{
		j = 0;
		printf("pair[%d] = ", i);
		do
		{
			x = number2[i][j];
			if (x != '\0') printf("%c", x);

			j++;
		} while (x != '\0');

		j = 0;
		printf("+");
	}
	*/


	//1
	// char to int
	for (i = 0; i < pair; i++)
	{
		j = 0;

		// check number length
		length = 0;
		negative = 0;
		do
		{
			x = number1[i][j];

			if ((0 == j) && (x == '-')) negative = 1;

			if (x != '\0') length++;

			j++;
		} while (x != '\0');

		// pairone[]
		number = 0;
		position = 0;
		for (k = length - 1; k > negative - 1; k--)
		{
			digit = char_to_int(number1[i][k]);

			if (k == (length - 1)) number = number + digit;// less significant
			else number = number + digit * position * 10;

			position++;
		}

		if (1 == negative) pairone[i] = (-1)*number;
		else pairone[i] = number;
	}


	//2
	// char to int
	for (i = 0; i < pair; i++)
	{
		j = 0;

		// check number length
		length = 0;
		negative = 0;
		do
		{
			x = number2[i][j];

			if ((0 == j) && (x == '-')) negative = 1;

			if (x != '\0') length++;

			j++;
		} while (x != '\0');


		// pairtwo[]
		number = 0;
		position = 0;
		for (k = length - 1; k > negative - 1; k--)
		{
			digit = char_to_int(number2[i][k]);

			if (k == (length - 1)) number = number + digit;// less significant
			else number = number + digit * position * 10;

			position++;
		}

		if (1 == negative) pairtwo[i] = (-1)*number;
		else pairtwo[i] = number;
	}


	//printf("\n");
	//for (i = 0; i < pair; i++) printf("%d, ", pairone[i]);
	//printf("\n");
	//for (i = 0; i < pair; i++) printf("%d, ", pairtwo[i]);
	//printf("\n");


	N = pair;


	// value
	for (i = 0; i < N; i++)
	{
		value[i] = pairone[i] + pairtwo[i];
	}

	// difference
	for (i = 1; i < N; i++)
	{
		if (value[i - 1] < 0)
		{
			value1 = (-1)*value[i - 1];
			neg1 = 1;
		}
		else value1 = value[i - 1];

		if (value[i] < 0)
		{
			value2 = (-1)*value[i];
			neg2 = 1;
		}
		else value2 = value[i];


		if ((neg1 = 0) && (neg2 == 0)) difference[i] = value2 - value1;

		if ((neg1 = 1) && (neg2 == 0)) difference[i] = value2 + value1;

		//if ((neg1 = 0) && (neg2 == 1)) difference[i] = value2 + value1;

		//if ((neg1 = 0) && (neg2 == 1)) difference[i] = value2 + value1;
	}

	// Are all the same?
	for (i = 0; i < N; i++)
	{
		if (value[0] != value[i]) allsame = 0;
	}

	// result
	if (1 == allsame) printf("Yes, value=%d", value[0]);
	else
	{
		// maxdiff
		maxdiff = difference[1];
		for (i = 2; i < N; i++)
		{
			if (difference[i] > difference[i - 1]) maxdiff = difference[i];
		}
		printf("No, maxdiff=%d", maxdiff);

		//maxdiff = max - min;
	}
}


int char_to_int(char x)
{
	switch (x)
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		default: return -1;
	}
}
