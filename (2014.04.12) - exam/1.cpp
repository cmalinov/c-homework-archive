//1-triangle

#include<stdio.h>
#include<conio.h>
#include<math.h>
#include <iostream>
using namespace std;

int main(void)
{
	float a, b, c, Ax, Ay, Bx, By, Cx, Cy;
	float p, S;

	//scanf_s("%f", &Ax);
	//scanf_s("%f", &Ay);
	//scanf_s("%f", &Bx);
	//scanf_s("%f", &By);
	//scanf_s("%f", &Cx);
	//scanf_s("%f", &Cy);

	cin >> Ax;
	cin >> Ay;
	cin >> Bx;
	cin >> By;
	cin >> Cx;
	cin >> Cy;

	//printf("\n\nAx = %.2f", Ax);


	// A-B
	a = sqrtf((Ay - Ax)*(Ay - Ax) + (By - Bx)*(By - Bx));
	// B-C
	b = sqrtf((Cy - Cx)*(Cy - Cx) + (By - Bx)*(By - Bx));
	// C-A
	c = sqrtf((Ay - Ax)*(Ay - Ax) + (Cy - Cx)*(Cy - Cx));

	//printf("\n\na = %.2f", a);

	p = (a + b + c) / 2;

	S = sqrtf(p*(p-a)*(p-b)*(p-c));

	if (((a + b) > c) && ((b + c) > a) && ((a + c) > b))
	{
		printf("Yes\n");
		printf("%.2f", S);
	}
	else
	{
		printf("No\n");
		printf("%.2f", a);
	}

}