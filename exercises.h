#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#define ARROW_UP 255+72
#define ARROW_DOWN 255+80

/* ============================================================================= */
/* EXERCISES */
/* ============================================================================= */
/* Unit 1 */
/* ============================================================================= */
/* modulus operator (%) */
void page_38_example_1(void)
{
	int a, exit = 0;

	do
	{
		system("cls");

		printf("Enter an integer: a = ");
		scanf("%d", &a);

		printf("\n%d%%2 = %d", a, a%2);
		printf("\n%d/2 = %d", a, a/2);

		printf("\n\nPress ESCAPE to quit the program or any other key to repeat it.");

		exit = getch();

	} while (exit!=27); /* until ESCAPE is pressed */
}
/* ----------------------------------------------------------------------------- */
/* area of rectangle */
void page_38_example_3(void)
{
	char asterisk[]={'*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
	int selection = 0;
	char *unit[]={"millimetre", "centimetre", "decimetre", "metre", "kilometre", "inch", "foot", "yard", "mile"};
	char *u[]={"mm", "cm", "dm", "m", "km", "in", "ft", "yd", "mi"};
	int i, j, exit = 0;
	float a, b;

	do
	{
		system("cls");
		printf("RECTANGLE AREA CALCULATOR\n\n");
		printf("Choose a unit of measurement by the UP and DOWN arrows.\n");
		printf("Press ENTER to confirm your choice.\n\n");
		printf("Unit of measurement: \n\n");

		for(i=0;i<9;i++)
		{
			printf("%c ", asterisk[i]);
			printf("%s\n", unit[i]);
		}
		
		exit = code_of_the_key_pressed();
		
		if (exit == ARROW_UP)
		{
			for (j=0;j<9;j++)
			{
				if (asterisk[j] == '*') 
				{
					asterisk[j] = ' ';
					if (j==0) selection = 8;
					else selection = j-1;
					asterisk[selection] = '*';
					
					/* produces a sound */
					printf("%c", 7);
					
					break;
				}
			}
		}
		else if (exit == ARROW_DOWN)
		{
			for (j=0;j<9;j++)
			{
				if (asterisk[j] == '*') 
				{
					asterisk[j] = ' ';
					if (j==8) selection = 0;
					else selection = j+1;		
					asterisk[selection] = '*';

					/* produces a sound */
					printf("%c", 7);
					
					break;
				}
			}		
		}

	} while (exit!=13); /* until ENTER is pressed */


	printf("\nEnter the length of one side of a recatngle: a = ");
	scanf("%f", &a);
	printf("Enter the length of the other side: b = ");
	scanf("%f", &b);

	printf("\nThe area of the rectangle is: S = %.2f %s^2", a*b, u[selection]);
}
/* ----------------------------------------------------------------------------- */
int code_of_the_key_pressed()
{
     int a;

	 /* The getch() function obtains the next character from the stream stdin. */
     a = getch();
	 if (a == 0 || a == 224) a = getch() + 255;
     
	 return a;
}
/* ----------------------------------------------------------------------------- */
void ASCII_table_0_255(void)
{
	int i;

	for (i=0; i<256; i++) 
	{
		printf("%3d %3c\n", i, i);

		/* The following is added to illustrate that ASCII code 7 is a sound: */
		/* getch(); */
	}
}
/* ----------------------------------------------------------------------------- */
void press_a_key_to_see_its_ASCII_code(void)
{
	int x = 0, j=0;

	system("cls");
	printf("INFO: Press ESCAPE two consecutive times to exit the program.");

	printf("\n\nPress a key to see its ASCII code: ");

	do
	{
		/* Something else is pressed after the ESCAPE, therefore the counter is reset. */
		if (x!=27) j = 0;

		x = getch();

		system("cls");
		printf("INFO: Press ESCAPE two consecutive times to exit the program.");

		printf("\n\nPress a key to see its ASCII code: ");

		if (x == 27) j++; /* ESCAPE is pressed */

		if (j<2)
		{
			if (x==0 || x==224) printf("%5d %5d", x, getch());
			else printf("%3d", x);
		}
	} while (j<2);
}
/* ----------------------------------------------------------------------------- */
/* volume of a cube */
void page_39_exercise_1(void)
{
	int i = 0, exit;
	float a;
	char *unit[]={"mm", "cm", "dm", "m", "km", "in", "ft", "yd", "mi"};

	printf("Choose a measurement unit by the keyboard arrows and confirm by ENTER: %s", unit[i]);
	do
	{
		exit = code_of_the_key_pressed();
		if (exit == ARROW_UP)
		{
			if (i==0) i=8;
			else i--;
		}
		if (exit == ARROW_DOWN)
		{
			if (i==8) i=0;
			else i++;
		}
		system("cls");
		printf("Choose a measurement unit by the keyboard arrows and confirm by ENTER: %s", unit[i]);
	} while (exit != 13);

	printf("\n\nEnter the length of a cube's side: ");
	scanf("%f", &a);

	printf("\nThe volume of the cube is: %f %s^3", a*a*a, unit[i]);
}
/* ----------------------------------------------------------------------------- */
/* count of seconds in a period of time */
void page_39_exercise_2(void)
{
	float t = 1, k;
	int choice;
	char *period_single[]={"minute", "hour", "day", "week", "year", "leap-year"};
	char *period_plural[]={"minutes", "hours", "days", "weeks", "year", "leap-year"};

	printf("Choose a measurement unit for a period of time:\n\n");
	printf("Press 1 for MINUTE.\n");
	printf("Press 2 for HOUR.\n");
	printf("Press 3 for DAY.\n");
	printf("Press 4 for WEEK.\n");
	printf("Press 5 for ONE YEAR.\n");
	printf("Press 6 for ONE LEAP-YEAR.\n");

	do
	{
		choice = code_of_the_key_pressed();
		switch (choice)
		{
			case 49: 
					k = 60; 
					printf("\nEnter the number of minutes: "); 
					scanf("%f", &t);
					break;
			case 50: 
					k = 60*60; 
					printf("\nEnter the number of hours: "); 
					scanf("%f", &t);
					break;
			case 51: 
					k = 60*60*24; 
					printf("\nEnter the number of days: "); 
					scanf("%f", &t);
					break;
			case 52:
					k = 60*60*24*7; 
					printf("\nEnter the number of weeks: "); 
					scanf("%f", &t);
					break;
			case 53: 
					k = 60*60*24*365; 
					break;
			case 54:
					k = 60*60*24*366; 
					break;
			default: break;
		}
	} while ((choice!=49)&&(choice!=50)&&(choice!=51)&&(choice!=52)&&(choice!=53)&&(choice!=54));

	if (t==1) printf("\nThere are %f seconds in %f %s.", t*k, t, period_single[(choice - 48) - 1]);
	else printf("\nThere are %f seconds in %f %s.", t*k, t, period_plural[(choice - 48) - 1]);
}
/* ----------------------------------------------------------------------------- */
void page_40_example_1(void)
{
	float d, y;

	printf("Enter the number of Earth days to be converted to jovian years: ");
	scanf("%f", &d);
	printf("\nEquivalent jovian years: %f", d/(365*12));
}
/* ----------------------------------------------------------------------------- */
void page_48_example_1(void)
{
	double x;

	printf("Enter a number to calculate its square: ");
	scanf("%lf", &x);

	printf("The square of %f is: %f", x, get_square(x));
}
/* ----------------------------------------------------------------------------- */
double get_square(double x)
{
	return x*x;
}
/* ----------------------------------------------------------------------------- */
void page_49_exercise_2(void)
{
	char *currency[]={"BGN", "EUR", "USD", "GBP", "CAD"};
	char rate_whole_number[4], rate_real_number[4];
	double rate, amount;
	int i = 0, x, f = 0, t = 0, counter, forward;

	printf("Use ENTER for confirmation of any choice!");


	printf("\n\nChoose a currency to convert from (use the keyboard arrows): %s", currency[f]);
	do
	{
		x = code_of_the_key_pressed();
		if (x == ARROW_UP)
		{
			if (f == 0) f = 4;
			else f--;
		}
		if (x == ARROW_DOWN)
		{
			if (f == 4) f = 0;
			else f++;
		}
		printf("\rChoose a currency to convert from (use the keyboard arrows): %s", currency[f]);
	} while (x!=13);


	if (f==0) t=1;
	printf("\nChoose a currency to convert to (use the keyboard arrows): %s", currency[t]);
	do
	{
		x = code_of_the_key_pressed();
		if (x == ARROW_UP)
		{
			if (t == 0) t = 4;
			else t--;

			if (t==f)
			{
				if (t == 0) t = 4;
				else t--;			
			}
		}
		if (x == ARROW_DOWN)
		{
			if (t == 4) t = 0;
			else t++;

			if (t==f)
			{
				if (t == 4) t = 0;
				else t++;
			}
		}
		printf("\rChoose a currency to convert to (use the keyboard arrows): %s", currency[t]);
	} while (x!=13);
	

	strcpy(rate_whole_number, currency[f]);
	strcpy(rate_real_number, currency[t]);
	counter = 1;
	forward = 1;
	printf("\nChoose conversion rate method (use the keyboard arrows): %s 1 = %s X", rate_whole_number, rate_real_number);
	do
	{
		x = code_of_the_key_pressed();
		counter++;
		if ((x == ARROW_UP)||(x==ARROW_DOWN))
		{
			
			if ((counter%2)==0)
			{
				strcpy(rate_whole_number, currency[t]);
				strcpy(rate_real_number, currency[f]);
				forward = 0;
			}
			else
			{
				strcpy(rate_whole_number, currency[f]);
				strcpy(rate_real_number, currency[t]);			
				forward = 1;
			}
		}
		printf("\rChoose conversion rate method (use the keyboard arrows): %s 1 = %s X", rate_whole_number, rate_real_number);
	} while (x!=13);


	printf("\nChoose conversion rate: %s 1 = %s ", rate_whole_number, rate_real_number);
	scanf("%lf", &rate);

	printf("\nChoose the amount to convert: %s ", currency[f]);
	scanf("%lf", &amount);

	if (forward == 1) printf("\n %s %f = %s %f", currency[f], amount, currency[t], amount*rate);
	else printf("\n %s %f = %s %f", currency[f], amount, currency[t], amount/rate);
}
/* ----------------------------------------------------------------------------- */
void exercise_with_float_numbers(void)
{
	float x;
	scanf("%f", &x); // input: 33.3
	printf("x = %f", x); // otput: 33.299999


    float y;
	scanf("%f", &y); // input: 33.3
	printf("x = %.5f", y); // otput: 33.30000
}
/* ----------------------------------------------------------------------------- */
void page_54_task_1(void)
{
	float m;

	printf("Enter your weight in [kg]: ");
	scanf("%f", &m);

	printf("Your weight on the Moon would be: %.2f kg", (m/100)*17);
}
/* ----------------------------------------------------------------------------- */
void page_54_task_3(void)
{
	float x;

	printf("Enter volume in oz: ");
	scanf("%f", &x);
	
	printf("%.2f oz equal %.2f cups.", x, oz_to_cups(x));
}
/* ----------------------------------------------------------------------------- */
float oz_to_cups(float v)
{
	/* A single cup contains 8 oz.*/
	return v/8;
}
/* ============================================================================= */
/* Unit 2 */
/* ============================================================================= */
void page_60_example_1(void)
{
	float a, b, S;

	printf("a = ");
	scanf("%f", &a);
	printf("b = ");
	scanf("%f", &b);

	printf("Answer the question: a + b = ");
	scanf("%f", &S);

	if (S==(a+b)) printf("\nThe answer is CORRECT");
	else printf("\nThe answer is WRONG. The correct answer is: a + b = %.4f", a+b);
}
/* ----------------------------------------------------------------------------- */
void page_61_example_2(void)
{
	int choice;
	float x;

	printf("Choose A or B:");
	printf("\n\nA: Convert from metres to feet.");
	printf("\n\nB: Convert from feet to metres.");

	do
	{
		choice = code_of_the_key_pressed();
	} while ((choice!=(int)'A')&&(choice!=(int)'a')&&(choice!=(int)'B')&&(choice!=(int)'b'));

	if ((choice==(int)'A')||(choice==(int)'a'))
	{
		printf("\n\nEnter length in [m]: ");
		scanf("%f", &x);
		printf("\n%.4f m = %.4f ft", x, x/0.3048);
	}
	else
	{
		printf("\n\nEnter length in [ft]: ");
		scanf("%f", &x);
		printf("\n%.4f ft = %.4f m", x, x*0.3048);	
	}
}
/* ----------------------------------------------------------------------------- */
void page_61_exercise_1(void)
{
	if (10*9<90) printf("(10*9<90) = TRUE");
	else printf("(10*9<90) = FALSE");

	if (10*(9<90)) printf("\nTRUE: 10*(9<90) = %d", 10*(9<90));
	else printf("\nFALSE: 10*(9<90) = %d", 10*(9<90));
}
/* ----------------------------------------------------------------------------- */
void page_61_exercise_2(void)
{
	int x;
	char answer[5];

	printf("Enter an integer number: ");
	scanf("%d", &x);

	if (x%2==0) strcpy(answer, "even");
	else strcpy(answer, "odd");

	printf("\n%d is an %s number.", x, answer);
}
/* ----------------------------------------------------------------------------- */
void page_66_exercise_1(void)
{
	int i, a, b, exit, selected = 0;
	char *operation[]={"add (a+b)", "subtract (a-b)", "multiply (a*b)", "divide (a/b)", "modulus operator division (a%b)"};
	char selection[]={'*', ' ', ' ', ' ', ' '};

	printf("Choose one of the following arithmetic operations (use the ARROW keys):\n");

	for (i = 0; i < 5; i++)
	{
		printf("\n%c %s", selection[i], operation[i]);
	}

	do
	{
		exit = code_of_the_key_pressed();

		if (exit == ARROW_UP)
		{
			/* sound signal */
			printf("\a");

			if (selected == 0) 
			{
				selection[selected] = ' ';
				selected = 4;
				selection[selected] = '*';
			}
			else 
			{
				selection[selected] = ' ';
				selected--;
				selection[selected] = '*';
			}
		}
		else if (exit == ARROW_DOWN)
		{
			/* sound signal */
			printf("\a");

			if (selected == 4) 
			{
				selection[selected] = ' ';
				selected = 0;
				selection[selected] = '*';			
			}
			else 
			{
				selection[selected] = ' ';
				selected++;
				selection[selected] = '*';			
			}
		}

		system("cls");
		printf("Choose one of the following arithmetic operations (use the ARROW keys):\n");

		for (i = 0; i < 5; i++)
		{
			printf("\n%c %s", selection[i], operation[i]);
		}
	} while (exit!=13);  /* until ENTER is pressed */

	printf("\n\nSpecify integer values for a and b:");
	printf("\na = ");
	scanf("%d", &a);

	if (selected > 2)
	{
		do
		{
			printf("\nb = ");
			scanf("%d", &b);
			if (b == 0) printf("b can not be equal to 0. Specify another value:");
		} while (b == 0);
	}
	else
	{
		printf("\nb = ");
		scanf("%d", &b);	
	}

	switch (selected)
	{
		case 0: printf("\na + b = %d + %d = %d", a, b, a+b); break;
		case 1: printf("\na - b = %d - %d = %d", a, b, a-b); break;
		case 2: printf("\na * b = %d * %d = %d", a, b, a*b); break;
		case 3: printf("\na / b = %d / %d = %.4f", a, b, (float)a/(float)b); break;
		case 4: printf("\na %% b = %d %% %d = %d", a, b, a%b); break;
		default: break;
	}
}
/* ----------------------------------------------------------------------------- */
void page_69_example_1(void)
{
	int i, x, answers = 0;

	for (i = 1; i <11; i++)
	{
		do
		{
			answers++;

			printf("\n%d + %d = ? \t Type your answer: ", i, i);
			scanf("%d", &x);

			if (x != 2*i) printf("WRONG ANSWER. Try again:");
		} while (x!=2*i);
	}
	printf("\n You gave %d wrong answers.", answers-10);
}
/* ----------------------------------------------------------------------------- */
void page_69_example_2(void)
{
	int x, i, prime = 1;

	printf("Enter a positive integer number greater than 1: ");
	scanf("%d", &x);

	printf("\nPositive divisors of %d: ", x);
	for (i = 2; i < x; i++)
	{
		if (x % i == 0) 
		{
			printf("%d ", i);
			prime = 0;
		}
	}

	if (prime == 1) printf("NONE. %d is a prime number.", x);
}
/* ----------------------------------------------------------------------------- */
void page_73_exercise_2(void)
{
	int a;

	a = 1;
	a = a + 16;
	printf("a = %d", a);

	a = 1;
	a += 16;
	printf("\na = %d", a);
}
/* ----------------------------------------------------------------------------- */
void numeral_systems_conversions(void)
{
	int x, i, selected = 0;
	char *conversions[] = { "binary to octal conversion",
							"binary to decimal conversion",
							"binary to hexadecimal conversion",
							"octal to binary conversion",
							"octal to decimal conversion",
							"octal to hexadecimal conversion",
							"decimal to binary conversion",
							"decimal to octal conversion",
							"decimal to hexadecimal conversion",
							"hexadecimal to binary conversion",
							"hexadecimal to octal conversion",
							"hexadecimal to decimal conversion" };
	char selection[] = {'*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};

	system("cls");
	printf("By the ARROW keys, choose a conversion method (ESCAPE to end the program):\n\n");

	for (i = 0; i < 12; i++)
	{
		printf("%c %s\n", selection[i], conversions[i]);
	}

	do
	{
		do
		{
			x = code_of_the_key_pressed();
			
			if (x == 27) break;

			if (x == ARROW_UP)
			{
				printf("\a");
				selection[selected] = ' ';

				if (selected == 0)
				{
					selected = 11;
					selection[selected] = '*';
				}
				else selection[--selected] = '*';
			}
			else if (x == ARROW_DOWN)
			{
				printf("\a");
				selection[selected] = ' ';

				if (selected == 11)
				{
					selected = 0;
					selection[selected] = '*';
				}
				else selection[++selected] = '*';
			}

			system("cls");
			printf("By the ARROW keys, choose a conversion method (ESCAPE to end the program):\n\n");

			for (i = 0; i < 12; i++)
			{
				printf("%c %s\n", selection[i], conversions[i]);
			}
		} while (x!=13);

		if (x == 27)
		{
			printf("\n\nEND OF THE PROGRAM.\n\n");
			break;
		}

		if (x==13)
		{
			switch (selected)
			{
				case 0: bin_to_oct_print(); break;
				case 1: bin_to_dec_print(); break;
				case 2: bin_to_hex_print(); break;
				case 3: oct_to_bin_print(); break;
				case 4: oct_to_dec_print(); break;
				case 5: oct_to_hex_print(); break;
				case 6: dec_to_bin_method_1_print(); break;
				case 7: dec_to_oct_print(); break;
				case 8: dec_to_hex_print(); break;
				case 9: hex_to_bin_print(); break;
				case 10: hex_to_oct_print(); break;
				case 11: hex_to_dec_print(); break;
				default: break;
			}
		}

	} while (x != 27);
}
/* ----------------------------------------------------------------------------- */
void bin_to_oct_print(void)
{
	int x, bin_length = 0, j, oct_position = 0, oct_length = 0;
	int bin_number[32];
	double y = 0, oct_number[11];
	
	printf("\nEnter a binary number: ");
	/* The most significant bit is bin_number[0]. */
	do
	{
		x = code_of_the_key_pressed();

		if (x == 48)
		{
			bin_number[bin_length++] = 0;
			printf("0");
		}
		else if (x == 49)
		{
			bin_number[bin_length++] = 1;
			printf("1");
		}
	} while ( !( ((x == 13)&&(bin_length > 0)) || (bin_length == 32) ) );

	for (j = bin_length - 1; j >= 0; j--)
	{
		if (oct_position == 3) oct_position = 0;

		y += (double)bin_number[j]*pow((double)2, double(oct_position));
		oct_position++;

		if ((oct_position == 3)||(j == 0))
		{
			oct_number[oct_length++] = y;
			y = (double)0;
		}
	}

	printf("\n\nThe octal number is: ");
	for (j = oct_length - 1; j >= 0; j--)
	{
		printf("%.0f", oct_number[j]);
	}
}
/* ----------------------------------------------------------------------------- */
void bin_to_dec_print(void)
{
	int x, i = 0, j;
	double y = 0;
	int number[32];

	printf("\nEnter a binary number: ");
	do
	{
		x = code_of_the_key_pressed();

		if (x == 48) 
		{
			number[i++] = 0;
			printf("0");
		}
		else if (x == 49)
		{
			number[i++] = 1;
			printf("1");
		}
	} while ( !( ((x == 13)&&(i > 0)) || (i == 32) ) );

	for (j = i-1; j > -1; j--)
	{
		y += (double)number[j]*pow((double)2, double(i-1-j));
	}
	printf("\n\nThe decimal number is: %.0f\n", y);
}
/* ----------------------------------------------------------------------------- */
void bin_to_hex_print(void)
{
	int x, bin_length = 0, i, j, k, non_significant_zero = 1;
	char bin_number[32];
	char conversion_set[][5] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
							  "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};

	char hex_set[]={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	char hex_number[][5] = {"0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000"};
	char *h;

	printf("\nEnter a binary number: ");
	bin_length = 0;
	do
	{
		x = code_of_the_key_pressed();

		if ((x == 48)||(x == 49)) 
		{
			if (x == 48) printf("0");
			else printf("1");

			bin_number[bin_length++] = x;
		}
	} while ( !( ((x == 13)&&(bin_length > 0)) || (bin_length == 32) ) );


	/* Placing of the binary data into the hexadecimal template: */
	i = 7;
	j = 3;
	for (k = bin_length - 1; k >=0; k--)
	{
		hex_number[i][j] = bin_number[k];
			
		if (j == 0) 
		{
			j = 3;
			i--;
		}
		else j--;
	}

	printf("\n\nThe hexadecimal number is: ");

	/* Printing of the hexadecimal data: */
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 16; j++)
		{
			if (strcmp(hex_number[i], conversion_set[j]) == 0) 
			{
				if (strcmp(hex_number[i], "0000") != 0) non_significant_zero = 0;
				if ((strcmp(hex_number[i], "0000") == 0) && (non_significant_zero == 1)) break;
				if (non_significant_zero == 0) printf("%c", hex_set[j]);
			}
		}
	}
}
/* ----------------------------------------------------------------------------- */
void oct_to_bin_print(void)
{
	int x, oct_length = 0, i;
	char binary_set[][4] = {"000", "001", "010", "011", "100", "101", "110", "111"};
	char octal_number[11];
	char bin_number[32];
	int non_significant_zero = 1;

	printf("\nEnter an octal number: ");
	do
	{
		x = code_of_the_key_pressed();

		if ((x>=48)&&(x<=55))
		{
			if (oct_length == 10)
			{
				if ((octal_number[0] >= 48)&&(octal_number[0] <= 51))
				{
					octal_number[oct_length++] = x;
					printf("%c", x);
				}
				else break;
			}
			else 
			{
				octal_number[oct_length++] = x;
				printf("%c", x);
			}
		}

	} while (!( ((x == 13)&&(oct_length > 0))||(x == 27)||(oct_length == 11) ));

	if (x != 27)
	{
		printf("\nThe binary number is: ");
		for (i = 0; i < oct_length; i++)
		{
			if (non_significant_zero == 1)
			{
				if ((octal_number[i] >= 48)&&(octal_number[i] <= 51))
				{
					switch (octal_number[i])
					{
						case 48: break;
						case 49: printf("1"); non_significant_zero = 0; break;
						case 50: printf("10"); non_significant_zero = 0; break;
						case 51: printf("11"); non_significant_zero = 0; break;
						default: break;
					}
				}
				else 
				{
					printf("%s", binary_set[octal_number[i]-48]);
					non_significant_zero = 0;
				}
			}
			else 
			{
				printf("%s", binary_set[octal_number[i]-48]);
			}
		}
	}
}
/* ----------------------------------------------------------------------------- */
void oct_to_dec_print(void)
{
	int x, oct_length = 0, max_length = 10, i;
	char octal_number[11];
	double dec_number = 0;

	printf("\nEnter octal number: ");
	do
	{
		x = code_of_the_key_pressed();

		if ((oct_length == 0)&&(x == 48)) continue;
		if ((x >= 48)&&(x <= 55))
		{
			if (oct_length == 0)
			{
				if ((x >= 48) && (x <= 51)) max_length == 11;
			}
			octal_number[oct_length++] = (char)x;
			printf("%c", x);
		}

	} while (!(((x == 13)&&(oct_length != 0))||(x == 27)||(oct_length == max_length)));

	
	for (i = oct_length-1; i >= 0; i--)
	{
		dec_number += (double)char_to_int(octal_number[i])*pow((double)8, (double)(oct_length-1-i));
	}

	printf("\nThe decimal number is: %.0f", dec_number);
}
/* ----------------------------------------------------------------------------- */
int char_to_int(char x)
{
	switch (x)
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		default: return -1;
	}
}
/* ----------------------------------------------------------------------------- */
/* 1. Octal to binary numeral system. */
/* 2. Binary to hexadecimal numeral system. */
void oct_to_hex_print(void)
{
	int x, i, max_oct_length = 10, oct_length = 0;
	char oct_number[11], bin_number[33];
	int bin_length;
	int j;
	char hex_buffer[5] = {'0', '0', '0', '0', '\0'}, hex_number[8];
	int hex_length = 0;

	printf("\nEnter an octal number: ");
	do
	{
		x = getch();
		if ((x == 0) || (x == 224)) x = 255 + getch();

		/* Entering of non significant zeros is not allowed. */

		if ((x >= 48) & (x <= 55))
		{
			/* The largest octal number with four bytes of lengths is 37777777777. */
			/* If the first digit entered is higher than 3, */
			/* then only a 10 digits long octal number can be placed within 4 bytes in memory. */
			/* If the first digit entered is lower than 4, */
			/* then an 11 digits long octal number can be placed within 4 bytes in memory. */
			if ((oct_length == 0) && (x <= 51)) max_oct_length = 11;

			/* octal_number[0] is the most significant bit. */
			oct_number[oct_length++] = (char)x;
			printf("%c", x);
		}
	} while (!(((x == 13)&&(oct_length != 0))||(x == 27)||(oct_length == max_oct_length)));

	if (x != 27)
	{
		/* Octal to binary numeral system conversion. */
		for (i = 0; i < oct_length; i++)
		{
			if (i == 0) /* The binary number will not contain non significant zeros. */
			{
				switch (oct_number[i])
				{
					case '0': break;
					case '1': strcpy(bin_number, "1"); break;
					case '2': strcpy(bin_number, "10"); break;
					case '3': strcpy(bin_number, "11"); break;
					case '4': strcpy(bin_number, "100"); break;
					case '5': strcpy(bin_number, "101"); break;
					case '6': strcpy(bin_number, "110"); break;
					case '7': strcpy(bin_number, "111"); break;
					default: break;
				}
			}
			else
			{
				switch (oct_number[i])
				{
					case '0': strcat(bin_number, "000"); break;
					case '1': strcat(bin_number, "001"); break;
					case '2': strcat(bin_number, "010"); break;
					case '3': strcat(bin_number, "011"); break;
					case '4': strcat(bin_number, "100"); break;
					case '5': strcat(bin_number, "101"); break;
					case '6': strcat(bin_number, "110"); break;
					case '7': strcat(bin_number, "111"); break;
					default: break;
				}		
			}
		}
		bin_length = strlen(bin_number);


		/* Binary to hexadecimal numeral system conversion. */
		j = 4;
		hex_length = 0;
		for (i = bin_length-1; i >= 0; i--)
		{
			hex_buffer[--j] = *(bin_number + i);
			if ((j == 0)||(i == 0))
			{
				if (strcmp(hex_buffer, "0000") == 0) hex_number[hex_length++] = '0';
				else if (strcmp(hex_buffer, "0001") == 0) hex_number[hex_length++] = '1';
				else if (strcmp(hex_buffer, "0010") == 0) hex_number[hex_length++] = '2';
				else if (strcmp(hex_buffer, "0011") == 0) hex_number[hex_length++] = '3';
				else if (strcmp(hex_buffer, "0100") == 0) hex_number[hex_length++] = '4';
				else if (strcmp(hex_buffer, "0101") == 0) hex_number[hex_length++] = '5';
				else if (strcmp(hex_buffer, "0110") == 0) hex_number[hex_length++] = '6';
				else if (strcmp(hex_buffer, "0111") == 0) hex_number[hex_length++] = '7';
				else if (strcmp(hex_buffer, "1000") == 0) hex_number[hex_length++] = '8';
				else if (strcmp(hex_buffer, "1001") == 0) hex_number[hex_length++] = '9';
				else if (strcmp(hex_buffer, "1010") == 0) hex_number[hex_length++] = 'A';
				else if (strcmp(hex_buffer, "1011") == 0) hex_number[hex_length++] = 'B';
				else if (strcmp(hex_buffer, "1100") == 0) hex_number[hex_length++] = 'C';
				else if (strcmp(hex_buffer, "1101") == 0) hex_number[hex_length++] = 'D';
				else if (strcmp(hex_buffer, "1110") == 0) hex_number[hex_length++] = 'E';
				else if (strcmp(hex_buffer, "1111") == 0) hex_number[hex_length++] = 'F';

				j = 4;
				strcpy(hex_buffer, "0000");
			}
		}

		printf("\nThe hexadecimal number is: ");
		for (i = hex_length - 1; i >= 0; i--)
		{
			printf("%c", hex_number[i]);
		}
	}
}
/* ----------------------------------------------------------------------------- */
void dec_to_bin_method_1_print(void)
{
	int x, y = 1, i, end;
	char binary_number[32];

	printf("\nEnter a decimal integer number: ");
	scanf("%d", &x);
/*
	do
	{
		y = y*2;
		if (y > x) 
		{
			end = 1;
			y = y/2;
		}
		else i++;
	} while


	while (y>1)
	{
	
	
	}
*/
	/*
	abcde
	bcdea
	cdeab
	deabc
	eabcd

	7:2-->1
	3:2-->1
	1:2-->1

	4:2-0
	2:2-0
	1:2-1
	

	9:2 -> 1
	4:2 -> 0
	2:2 -> 1
	1:


	((x) < LONG_MIN-0.5 || (x) > LONG_MAX+0.5) ? error() : ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))
*/
	
}
/* ----------------------------------------------------------------------------- */
void dec_to_bin_method_2_print(void)
{
	/*
	1   2   4   8   16   32   64   128   256   512   1024


	*/
}
/* ----------------------------------------------------------------------------- */
void dec_to_oct_print(void)
{
	int x;

	printf("\nEnter a decimal integer number: ");
	scanf("%d", &x);
}
/* ----------------------------------------------------------------------------- */
void dec_to_hex_print(void)
{
	int x;
	
	printf("\nEnter a decimal integer number: ");
	scanf("%d", &x);
}
/* ----------------------------------------------------------------------------- */
void hex_to_bin_print(void)
{

}
/* ----------------------------------------------------------------------------- */
/* 1. Hexadecimal to binary numeral system. */
/* 2. Binary to octal numeral system. */
void hex_to_oct_print(void)
{

}
/* ----------------------------------------------------------------------------- */
void hex_to_dec_print(void)
{

}
/* ----------------------------------------------------------------------------- */
void char_pointer_to_int_example(void)
{
	int i = 128, *p_i;
	char *p_c;
	
	printf("Enter an integer number: ");
	scanf("%d", &i);

	p_i = &i;
	p_c = (char*)p_i;

	printf("\n*p_i: %d", *p_i);
	printf("\n*p_c: %d", *p_c);
}
/* ----------------------------------------------------------------------------- */
void data_types_print(void)
{
	double x, y;
	int i;
	signed char c;
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* char */
	printf("%26s %d %s\t", "sizeof(char)", sizeof(char), (sizeof(char) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(char)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(char)*8-1); i++) 
	{
		y += pow((double)2, (double)(sizeof(char)*8-1-1-i));
	}
	printf("MIN: %11.0d     MAX: %.0d\n", c = (int)x, (int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* unsigned char */
	printf("%26s %d %s\t", "sizeof(unsigned char)", sizeof(unsigned char), (sizeof(unsigned char) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(unsigned char)*8-1));
	y = x;
	for(i = 0; i < (sizeof(unsigned char)*8-1); i++) 
	{
		y += pow((double)2, (double)(sizeof(unsigned char)*8-1-1-i));
	}
	printf("MIN:           0     MAX: %.0d\n", (int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* signed char */
	printf("%26s %d %s\t", "sizeof(signed char)", sizeof(signed char), (sizeof(signed char) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(unsigned char)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(unsigned char)*8-1); i++) 
	{
		y += pow((double)2, (double)(sizeof(unsigned char)*8-1-1-i));
	}
	printf("MIN: %11.0d     MAX: %.0d\n", c = (int)x, (int)y);
	
	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* char* */
	printf("%26s %d %s\n", "sizeof(char*)", sizeof(char*), (sizeof(char*) == 1)?"byte":"bytes");

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* char** */
	printf("%26s %d %s\n", "sizeof(char**)", sizeof(char**), (sizeof(char**) == 1)?"byte":"bytes");
	
	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* int */
	printf("%26s %d %s\t", "sizeof(int)", sizeof(int), (sizeof(int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(int)*8-1-1-i));
	}
	printf("MIN: %11.0d     MAX: %.0d\n", (int)x, (int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* unsigned int */
	printf("%26s %d %s\t", "sizeof(unsigned int)", sizeof(unsigned int), (sizeof(unsigned int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(unsigned int)*8-1));
	y = x;
	for(i = 0; i < (sizeof(unsigned int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(unsigned int)*8-1-1-i));
	}
	printf("MIN:           0     MAX: %.0u\n", (unsigned int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* signed int */
	printf("%26s %d %s\t", "sizeof(signed int)", sizeof(signed int), (sizeof(signed int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(signed int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(signed int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(signed int)*8-1-1-i));
	}
	printf("MIN: %11.0d     MAX: %.0d\n", (signed int)x, (signed int)y);

	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* short int */
	printf("%26s %d %s\t", "sizeof(short int)", sizeof(short int), (sizeof(short int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(short int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(short int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(short int)*8-1-1-i));
	}
	printf("MIN: %11.0hd     MAX: %.0hd\n", (short int)x, (short int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* unsigned short int */
	printf("%26s %d %s\t", "sizeof(unsigned short int)", sizeof(unsigned short int), (sizeof(unsigned short int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(unsigned short int)*8-1));
	y = x;
	for(i = 0; i < (sizeof(unsigned short int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(unsigned short int)*8-1-1-i));
	}
	printf("MIN:           0     MAX: %.0hu\n", (unsigned short int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* signed short int */
	printf("%26s %d %s\t", "sizeof(signed short int)", sizeof(signed short int), (sizeof(signed short int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(signed short int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(signed short int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(signed short int)*8-1-1-i));
	}
	printf("MIN: %11.0hd     MAX: %.0hd\n", (signed short int)x, (signed short int)y);

	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* long int */
	printf("%26s %d %s\t", "sizeof(long int)", sizeof(long int), (sizeof(long int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(long int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(long int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(long int)*8-1-1-i));
	}
	printf("MIN: %11.0ld     MAX: %.0ld\n", (long int)x, (long int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* unsigned long int */
	printf("%26s %d %s\t", "sizeof(unsigned long int)", sizeof(unsigned long int), (sizeof(unsigned long int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(unsigned long int)*8-1));
	y = x;
	for(i = 0; i < (sizeof(unsigned long int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(unsigned long int)*8-1-1-i));
	}
	printf("MIN:           0     MAX: %.0lu\n", (unsigned long int)y);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* signed long int */
	printf("%26s %d %s\t", "sizeof(signed long int)", sizeof(signed long int), (sizeof(signed long int) == 1)?"byte":"bytes");
	x = pow((double)2, (double)(sizeof(signed long int)*8-1));
	y = 0;
	for(i = 0; i < (sizeof(signed long int)*8-1); i++)
	{
		y += pow((double)2, (double)(sizeof(signed long int)*8-1-1-i));
	}
	printf("MIN: %11.0ld     MAX: %.0ld\n", (signed long int)x, (signed long int)y);

	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* int* */
	printf("%26s %d %s\n", "sizeof(int*)", sizeof(int*), (sizeof(int*) == 1)?"byte":"bytes");

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* int** */
	printf("%26s %d %s\n", "sizeof(int**)", sizeof(int**), (sizeof(int**) == 1)?"byte":"bytes");
	
	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* float */
	printf("%26s %d %s\n", "sizeof(float)", sizeof(float), (sizeof(float) == 1)?"byte":"bytes");

	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* float* */
	printf("%26s %d %s\n", "sizeof(float*)", sizeof(float*), (sizeof(float*) == 1)?"byte":"bytes");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* float** */
	printf("%26s %d %s\n", "sizeof(float**)", sizeof(float**), (sizeof(float**) == 1)?"byte":"bytes");
	
	printf("\n");

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* double */
	printf("%26s %d %s\n", "sizeof(double)", sizeof(double), (sizeof(double) == 1)?"byte":"bytes");

	printf("\n");

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* long double */
	printf("%26s %d %s\n", "sizeof(long double)", sizeof(long double), (sizeof(long double) == 1)?"byte":"bytes");

	printf("\n");
	
	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* double* */
	printf("%26s %d %s\n", "sizeof(double*)", sizeof(double*), (sizeof(double*) == 1)?"byte":"bytes");

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/* double** */
	printf("%26s %d %s\n", "sizeof(double**)", sizeof(double**), (sizeof(double**) == 1)?"byte":"bytes");
}
/* ----------------------------------------------------------------------------- */
void debugger_exercise(void)
{
	int i, a;

	for (i = 0; i < 10; i++)
	{
		printf("i = %d\n", i);
		a = i;
		printf("a = %d\n", a);
	}
}
/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 3 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 4 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 5 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 6 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 7 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 8 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 9 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 10 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 11 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit 12 */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit A */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit B */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* Unit C */
/* ============================================================================= */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------- */

/* ============================================================================= */
/* ============================================================================= */