#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C Operators\n\n");
	printf("Enter a number between 1 and 14 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem1(); break;
		case 2: problem2(); break;
		case 3: problem3(); break;
		case 4: problem4();	break;
		case 5: problem5(); break;
		case 6: problem6(); break;
		case 7: problem7(); break;
		case 8: problem8(); break;
		case 9: problem9(); break;
		case 10: problem10(); break;
		case 11: problem11(); break;
		case 12: problem12(); break;
		case 13: problem13(); break;
		case 14: problem14(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem1(void)
{
	int n;

	printf("\n\nn = ");
	scanf("%d", &n);

	printf("Odd? Result: %d", (n % 2)?1:0);
}
//========================================================================================================
void problem2(void)
{
	int n;

	printf("\n\nn = ");
	scanf("%d", &n);

	printf("Result: %d", ((n % 2) && (n > 20))?1:0);
}
//========================================================================================================
void problem3(void)
{
	int n;

	printf("\n\nn = ");
	scanf("%d", &n);

	printf("Result: %d", (!((n % 9) || (n % 11) || (n % 13)))?1:0);
}
//========================================================================================================
void problem4(void)
{
	float weight;

	printf("\n\nWeight on Earth = ");
	scanf("%f", &weight);

	printf("Weight in the moon: %.4f", (weight * 17) / 100);
}
//========================================================================================================
void problem5(void)
{
	int n;

	printf("\n\nn = ");
	scanf("%d", &n);

	printf("Divided by 7 and 5? Result: %d", !((n % 7) || (n % 5)));
}
//========================================================================================================
void problem6(void)
{
	float width, height;

	printf("\n\nwidth = ");
	scanf("%f", &width);

	printf("height = ");
	scanf("%f", &height);

	printf("\nperimeter = %f", 2 * (width + height));
	printf("\narea = %f", width * height);
}
//========================================================================================================
void problem7(void)
{
	float a, b, c;

	printf("\n\na = ");
	scanf("%f", &a);

	printf("b = ");
	scanf("%f", &b);

	printf("c = ");
	scanf("%f", &c);

	printf("\nAverage = %f", (a + b + c) / 3);
}
//========================================================================================================
void problem8(void)
{
	int n;

	printf("\n\nn = ");
	scanf("%d", &n);

	printf("Third digit 7? Result: %d", (700 == ((n % 1000) - (n % 100))));
}
//========================================================================================================
void problem9(void)
{

}
//========================================================================================================
void problem10(void)
{

}
//========================================================================================================
void problem11(void)
{

}
//========================================================================================================
void problem12(void)
{

}
//========================================================================================================
void problem13(void)
{

}
//========================================================================================================
void problem14(void)
{

}
//========================================================================================================
