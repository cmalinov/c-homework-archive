#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: C file processing\n\n");
	printf("Enter a number between 1 and 9 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem_1(); break;
		case 2: problem_2(); break;
		case 3: problem_3(); break;
		case 4: problem_4(); break;
		case 5: problem_5(); break;
		case 6: problem_6(); break;
		case 7: problem_7(); break;
		case 8: problem_8(); break;
		case 9: problem_9(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	FILE *file;
	char ch;

	printf("\nProblem 1. Print file contents. \n\n");

	file = fopen("main.c", "r");
	if (!file) return;

	do
	{
		ch = fgetc(file);
		printf("%c", ch);
	} while (EOF != ch);

	fclose(file);
}
//========================================================================================================
void problem_2(void)
{
	FILE *file;
	char ch;
	int odd = 1;

	printf("\nProblem 2. Odd lines. \n\n");

	file = fopen("main.c", "r");
	if (!file) return;

	do
	{
		ch = fgetc(file);

		if (1 == odd) printf("%c", ch);
		
		if ('\n' == ch)
		{
			if (0 == odd) odd = 1;
			else odd = 0;
		}
	} while (EOF != ch);

	fclose(file);
}
//========================================================================================================
void problem_3(void)
{
	FILE *file, *modified;
	char ch, last;
	int i = 0;
	char label[100];

	printf("\nProblem 3. Line numbers. \n\n");

	file = fopen("main.c", "r");
	if (!file) return;

	modified = fopen("modified.c", "w");
	if (!modified)
	{
		fclose(file);
		return;
	}
	
	sprintf(label, "%4d ", i++);
	fputs(label, modified);
	do
	{
		ch = fgetc(file);
		
		if (EOF != ch)
		{
			if ('\n' == last)
			{
				sprintf(label, "%4d ", i++);
				fputs(label, modified);
			}

			fputc(ch, modified);
		}
		last = ch;
	} while (EOF != ch);

	fclose(file);
	fclose(modified);
}
//========================================================================================================
void problem_4(void)
{
	FILE *file, *modified;
	size_t readBytes;
	int size = 4096, i = 0;
	char buffer[4096];

	printf("\nProblem 4. Copy a binary file. \n\n");

	file = fopen("scholars.jpg", "rb");
	if (!file) return;

	modified = fopen("scholars_copy.jpg", "wb");
	if (!modified)
	{
		printf("No copy created.");
		fclose(file);
		return;
	}
	
	while (!(feof(file)))
	{
		readBytes = fread(buffer, 1, size, file);
		fwrite(buffer, 1, readBytes, modified);
		i++;
	}

	printf("\n%d", i);

	fclose(file);
	fclose(modified);
}
//========================================================================================================
void problem_5(void)
{

}
//========================================================================================================
void problem_6(void)
{

}
//========================================================================================================
void problem_7(void)
{

}
//========================================================================================================
void problem_8(void)
{

}
//========================================================================================================
void problem_9(void)
{

}
//========================================================================================================



