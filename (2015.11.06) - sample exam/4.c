// INFO:
// https://www.securecoding.cert.org/confluence/display/c/FIO14-C.+Understand+the+difference+between+text+mode+and+binary+mode+with+file+streams
// 
//
// https://en.wikipedia.org/wiki/C_file_input/output
// http://www.cprogramming.com/tutorial/cfileio.html
//
// Reading from and writing to the middle of a binary file:
// http://stackoverflow.com/questions/1695681/reading-from-and-writing-to-the-middle-of-a-binary-file-in-c-c
//
// How can I get a file's size in C?
// http://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
//
// Can fseek() be used to insert data into the middle of a file?
// http://stackoverflow.com/questions/13435955/can-fseek-be-used-to-insert-data-into-the-middle-of-a-file-c

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
//#include <sys/stat.h>




int main(int argc, char** argv)
{
	//char nameSrc[500], nameDest[500];
	char *nameSrc, *nameDest;
	FILE *fileSrc, *fileDest;

	long srcLength, readBytes;
	char *buffer;
	char buff[4096], temp[4096];
	int chunk = 4096;
	
	int sizeA, sizeB;

	//int i = 0;

    //struct stat st; 
	//int srcSize;
    //if (stat(nameSrc, &st) == 0) srcSize = st.st_size;


	
	if (3 != argc)
	{
		printf("Usage: <src-file> <dest-file>");
		exit(EXIT_FAILURE);
	}
	nameSrc = argv[1];
	nameDest = argv[2];
	


	//strcpy(nameSrc, "src.txt");
	//strcpy(nameDest, "dest.txt");

	//printf("\nnameSrc = %s", nameSrc);
	//printf("\nnameDest = %s", nameDest);


	errno = 0;
	fileSrc = fopen(nameSrc, "rb");
	if (!fileSrc)
	{
		if (ENOENT == errno) printf("%s: No such file or directory", nameSrc);
		exit(EXIT_FAILURE);
	}

	errno = 0;
	fileDest = fopen(nameDest, "wb");
	if (!fileDest)
	{
		printf("The file %s is not opened for writing.", nameDest);
		fclose(fileSrc);
		exit(EXIT_FAILURE);
	}

	
	// SWAPPING CHUNKS:
	while (!(feof(fileSrc)))
	{
		// Get the next chunk into an array buffer
		readBytes = fread(buff, 1, chunk, fileSrc);
		sizeA = readBytes / 2;
		sizeB = readBytes - sizeA;

		// Swap the halves of the buffer
		memcpy(temp, buff + sizeA, sizeB);
		memcpy(temp + sizeB, buff, sizeA);
		
		// Write the swapped chunk into the destination file
		fwrite(temp, 1, readBytes, fileDest);
	}
	

	/*
	// THE FOLLOWING SWAPS THE HALVES OF THE WHOLE FILE:
	fseek(fileSrc, 0, SEEK_END);
	srcLength = ftell(fileSrc);
	//printf("\nsrcLength = %ld\n", srcLength);
	
	buffer = (char*)malloc((srcLength / 2) + 1);
	if (!buffer)
	{
		printf("The memory neccessary cannot be allocated.");
		exit(EXIT_FAILURE);
	}
	//fseek(fileSrc, 0, SEEK_SET);
	fseek(fileSrc, srcLength / 2, SEEK_SET);
	fread(buffer, 1, (srcLength - (srcLength / 2)), fileSrc);
	fwrite(buffer, 1, (srcLength - (srcLength / 2)), fileDest);
	fseek(fileSrc, 0, SEEK_SET);
	fread(buffer, 1, (srcLength / 2), fileSrc);
	fwrite(buffer, 1, (srcLength / 2), fileDest);

	free(buffer);
	*/


	/*
	// THE FOLLOWING COPIES THE CONTENT TO THE DESTINATION FILE:
	while (!(feof(fileSrc)))
	{
		readBytes = fread(buff, 1, 5, fileSrc);
		fwrite(buff, 1, readBytes, fileDest);
		i++;
	}
	printf("\ni = %d\n",i);
	*/


	fclose(fileSrc);
	fclose(fileDest);

	printf("Success!");

    return (EXIT_SUCCESS);
}


