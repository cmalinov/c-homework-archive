#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char * read_line(void);

int main(void)
{
	char *input;
	char *searchIndex;

	char *wordStart, *wordEnd, *border;

	char *output;
	int outputSize;

	int nextFind = 0;
	int exit = 0;
	
	char *remainder1, *remainder2, *remainder3;
	long positionA, positionB, size;
	int i;
	char temp;


	output = (char*)malloc(1);
	if (!output) return -1;
	output[0] = '\0';
	outputSize = 1;



	// INPUT
	input = read_line();

	// FIND ALL WORDS
	nextFind = 0;
	searchIndex = input;
	while(1)
	{
		wordStart = strchr(searchIndex, '{');
		if (NULL != wordStart)
		{
			wordEnd = strchr(searchIndex, '}');
			if (NULL != wordEnd)
			{
				border = strchr(wordStart, '|');
				if (((NULL == border) || (border > wordEnd)) && (1 < (wordEnd - wordStart)))
				{
					// A VALID WORD IS FOUND => extract it and stick it to the resulting text
					wordEnd[0] = '\0';

					output = (char*)realloc(output, strlen(output) + strlen(wordStart + 1) + 1);
					if (!output)
					{
						printf("The memory neccessary cannot be allocated.");
						return -1;
					}
					strcat(output, wordStart + 1);
				}

				// PREPARATION FOR THE NEXT SEARCH
				searchIndex = wordEnd + 1;
			}
			else break; // No more words to extract.
		}
		else break; // No more words to extract.
	}
	free(input);


	printf("--->%s", output);

	do
	{
		// INPUT
		input = read_line();


		// PROCESSING
		if (input == strstr(input, "swap "))
		//if ('s' == input[0])
		{
			positionA = strtol(input + 5, &remainder1, 10);
			positionB = strtol(remainder1, &remainder2, 10);
			size = strtol(remainder2, &remainder3, 10);

			// VALIDATION
			if ((0 > positionA) || (0 > positionB) || (0 > size) || 
				(strlen(output) < (positionA + size)) || (strlen(output) < (positionB + size)) ||
				(positionB < (positionA + size)) || (positionA > positionB))
			{
				printf("Invalid command parameters\n");
			}
			else // MODIFY THE TEXT
			{
				// SWAP
				for (i = 0; i < size; i++)
				{
					temp = output[positionA + i];
					output[positionA + i] = output[positionB + i];
					output[positionB + i] = temp;
				}
			}
		}
		else if (0 == strcmp(input, "end"))
		//else if ('e' == input[0])
		{
			exit = 1;
		}
		else
		{
			// Incorrect input.
		}

		free(input);
	} while (0 == exit);

	printf("%s", output);

	free(output);

	return 1;
}




char * read_line(void)
{
	int i = 0;
	char c;
	char *p;
	int size = 1024;

	p = (char *)malloc(size * sizeof(char));
	if (!p)
	{
		printf("The memory neccessary cannot be allocated.");
		return NULL;
	}


	do
	{
		// Allocation of more memory if the currently allocated is consumed.
		if ((i + 1) == size)
		{
			size += 1024;
			p = (char *)realloc(p, size);
			if (!p)
			{
				printf("The memory neccessary cannot be allocated.");
				return p;
			}
		}

		//c = getc(stdin);
		c = getchar();

		if (('\n' == c) || (EOF == c)) p[i] = '\0';
		else p[i++] = c;
	} while (('\n' != c) && (EOF != c));

	return p;
}