#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<prototypes.h>


int main(void)
{
	int choice;

	printf("Homework: exercises\n\n");
	printf("Enter a number between 1 and 9 for the corresponding Problem: ");

	scanf("%d", &choice);
	switch (choice)
	{
		case 1: problem_1(); break;
		case 2: problem_2(); break;
		case 3: problem_3(); break;
		case 4: problem_4(); break;
		case 5: problem_5(); break;
		case 6: problem_6(); break;
		case 7: problem_7(); break;
		case 8: problem_8(); break;
		case 9: problem_9(); break;
		default: printf("\nIncorrect input."); break;
	}


	printf("\n\n");	
	return 0;
}
//========================================================================================================
void problem_1(void)
{
	char *inputString;
	char text[5000];
	int x = 0;

	printf("\nProblem 1. get_string function. \n\n");
	fflush(stdout);
	

	// VARIANT 1
	//
	// Instead of fflush(stdin):
	while ((EOF != x) && ('\n' != x))
	{
		x = getchar();
	}
	//
	printf("Input string 1: ");
	inputString = get_string();
	if (inputString)
	{
		printf("String 1 is: %s\n", inputString);
	}
	free(inputString);
	

	// VARIANT 2
	//
	// Instead of fflush(stdin):
	while ((EOF != x) && ('\n' != x))
	{
		x = getchar();
	}
	//
	printf("\n\nInput string 2: ");
	text[0] = getchar();
	fgets(text + 1, 1000, stdin);
	printf("\nString 2 is: %s\n", text);

	/*
	http://rabbit.eng.miami.edu/class/een218/getchar.html
	
	http://stackoverflow.com/questions/22901901/what-does-fflushstdin-do-in-c-programing

	http://stackoverflow.com/questions/358342/canonical-vs-non-canonical-terminal-input

	http://stackoverflow.com/questions/7898215/how-to-clear-input-buffer-in-c
	*/
}

char* get_string(void)
{
	char *line, *resized;
	char ch;
	int length = 100, i = 0;

	line = (char*)malloc(length);
	if (!line) return NULL;

	// User's input in the stdin buffer until ENTER is pressed. Then the first character of the buffer is assigned to ch.
	ch = getchar();
	//
	// Analysing the rest of stdin buffer.
	while (('\n' != ch) && (EOF != ch))
	{
		if (length - 1 == i)
		{
			resized = (char*)realloc(line, length * 2);
			if (!resized)
			{
				line[i] = '\0';
				return line;
			}
			line = resized;
			length *= 2;
		}

		line[i++] = ch; // Every simbol in the buffer until '\n' (or EOF) is put in the destination string.
		ch = getchar();
	}

	line[i] = '\0';
	return line;
}
//========================================================================================================
// 13 : 2 => 6 ; 1
//  6 : 2 => 3 ; 0
//  3 : 2 => 1 ; 1
//  1 : 2 => 0 ; 1 => 1101
void problem_2(void)
{
	int x, i = 0;
	int bin[100];

	printf("\nProblem 2. Convert DEC to BIN. \n\n");
	fflush(stdout);

	// Instead of fflush(stdin):
	while ((EOF != x) && ('\n' != x))
	{
		x = getchar();
	}
	printf("x = ");
	scanf("%d", &x);

	// It is assumed that an integer number is correctly entered.
	if (2 > x)
	{
		printf("x[bin] = %d", x);
		return;
	}

	// DEC TO BIN
	do
	{
		bin[i++] = x % 2;
		x = x / 2;
	} while(0 != x);
	i--;
	bin[i] = 1;

	// PRINT BIN RESULT
	printf("x[bin] = ");
	do
	{
		printf("%d", bin[i--]);
	}
	while (0 <= i);
}
//========================================================================================================
void problem_3(void)
{
	char text[100];
	int bin[100];
	int x, i = 0, size, b = 1, j;

	printf("\nProblem 3. Convert BIN to DEC. \n\n");
	fflush(stdout);

	// Instead of fflush(stdin):
	while ((EOF != x) && ('\n' != x))
	{
		x = getchar();
	}
	printf("x[bin] = ");
	scanf("%s", &text); // It is assumed that the input string correctly contains only 0s or 1s.

	size = strlen(text);

	x = 0;
	for(i = 1; i <= size; i++)
	{
		b = 1;
		if ('1' == text[size - i])
		{
			for (j = 0; j < i - 1; j++) b *= 2;
			x = x + b;
		}
	}
	printf("x[dec] = %d", x);
}
//========================================================================================================
void problem_4(void)
{
	printf("\nProblem 4. Convert DEC to HEX. \n\n");
}
//========================================================================================================
void problem_5(void)
{
	printf("\nProblem 5. Convert HEX to DEC. \n\n");
}
//========================================================================================================
void problem_6(void)
{
	printf("\nProblem 6. Convert DEC to OCT. \n\n");
}
//========================================================================================================
void problem_7(void)
{
	printf("\nProblem 7. Convert OCT to DEC. \n\n");
}
//========================================================================================================
void problem_8(void)
{
	printf("\nProblem 8. Convert BIN to HEX. \n\n");
}
//========================================================================================================
void problem_9(void)
{
	printf("\nProblem 9. Convert HEX to BIN. \n\n");
}
//========================================================================================================



