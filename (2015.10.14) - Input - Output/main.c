#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<prototypes.h>

struct employee
{
	char firstName[100];
	char lastName[100];
	char age[4];
	char phone[30];
};


struct organization
{
	char name[200];
	char address[200];
	char phone[30];
	char fax[30];
	char website[200];

	struct employee manager;
} company;




int main(void)
{
	int choice;

	printf("Homework: Formatted Input / Output\n\n");
	printf("Press 1 for Problem 1\n");
	printf("Press 2 for Problem 2\n");
	printf("Press 3 for Problem 3\n");
	printf("Press 4 for Problem 4\n\n\nProblem ");


	do
	{
		choice = code_of_the_key_pressed();
		switch (choice)
		{
			case 49: problem1();
					 break;

			case 50: problem2();
					 break;

			case 51: problem3();
					 break;

			case 52: problem4();
					 break;

			default: break;
		}
	} while ((choice != 49) && (choice != 50) && (choice != 51) && (choice != 52));


	printf("\n\n");	
	return 0;
}
//========================================================================================================
int code_of_the_key_pressed()
{
     int a;

	 // The getch() function obtains the next character from the stream stdin.
     a = getch();
	 if (a == 0 || a == 224) a = getch() + 255;
     
	 return a;
}
//========================================================================================================
void problem1(void)
{
	double number1, number2, number3;

	printf("1: Sum of 3 Numbers\n\n");

	printf("Enter the first number: ");
	scanf("%lf", &number1);
	printf("Enter the second number: ");
	scanf("%lf", &number2);
	printf("Enter the third number: ");
	scanf("%lf", &number3);
	printf("The sum of the three numbers is: %lf.", number1 + number2 + number3);
}
//========================================================================================================
void problem2(void)
{
	printf("2: Print Company Information");

	printf("\n\nCompany name: ");
	gets(company.name);

	printf("Company address: ");
	gets(company.address);

	printf("Phone number: ");
	gets(company.phone);

	printf("Fax number: ");
	gets(company.fax);

	printf("Web site: ");
	gets(company.website);

	printf("Manager first name: ");
	gets(company.manager.firstName);

	printf("Manager last name: ");
	gets(company.manager.lastName);

	printf("Manager age: ");
	gets(company.manager.age);

	printf("Manager phone: ");
	gets(company.manager.phone);

	if (!strcmp("\0",company.name)) strcpy(company.name, "(no name)");
	if (!strcmp("\0",company.address)) strcpy(company.address, "(no address)");
	if (!strcmp("\0",company.phone)) strcpy(company.phone, "(no phone)");
	if (!strcmp("\0",company.fax)) strcpy(company.fax, "(no fax)");
	if (!strcmp("\0",company.website)) strcpy(company.website, "(no website)");
	if (!strcmp("\0",company.manager.firstName)) strcpy(company.manager.firstName, "(no name)");
	if (!strcmp("\0",company.manager.lastName)) strcpy(company.manager.lastName, "(no name)");
	if (!strcmp("\0",company.manager.age)) strcpy(company.manager.age, "(no age)");
	if (!strcmp("\0",company.manager.phone)) strcpy(company.manager.phone, "(no phone)");

	printf("\n%s\nAddress: %s", company.name, company.address);
	printf("\nTel. %s\nFax: %s\nWeb site: %s", company.phone, company.fax, company.website);
	printf("\nManager: %s %s", company.manager.firstName, company.manager.lastName);
	printf(" (age: %s, tel. %s)", company.manager.age, company.manager.phone);
}
//========================================================================================================
void problem3(void)
{
	double r = 0.0;
	double pi = 3.14159265359;

	printf("3: Circle Perimeter and Area");
	printf("\n\nr = ");
	scanf("%lf", &r);

	printf("\nperimeter = %.2lf", 2*pi*r);
	printf("\narea = %.2lf", pi*r*r);
}
//========================================================================================================
void problem4(void)
{
	int a;
	double b, c;
	char *format = "|%-10X|%10s|%10.2lf|%-10.3lf|\n";

	printf("4: Formatting Numbers");
	printf("\n\na = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%lf", &b);
	printf("c = ");
	scanf("%lf", &c);

	printf(format, a, "binary-TBD", b, c);
}
//========================================================================================================
