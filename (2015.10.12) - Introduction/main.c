#include<stdio.h>
#include<math.h>


int main(void)
{
	char name1[200], name2[200];
	double number;
	int i, n, element;

	// Problem 1: Play with Your IDE / Text Editor.

	// Problem 2: Play with Standard C Library.

	// Problem 3: Hello World
	printf("Problem 3 -------------------------------------------------------------------");
	printf("\nHello, C!");
	
	// Problem 4: Print Your Name
	printf("\n\nProblem 4 -------------------------------------------------------------------");
	printf("\nEnter your first name: ");
	scanf("%s", &name1);
	printf("Enter your surname: ");
	scanf("%s", &name2);
	printf("Your name is %s %s.", name1, name2);

	// Problem 5: Print Numbers
	printf("\n\nProblem 5 -------------------------------------------------------------------");
	printf("\n1\n101\n1001");

	// Problem 6: Print First and Last Name
	printf("\n\nProblem 6 -------------------------------------------------------------------");
	printf("\n%s\n%s", name1, name2);

	// Problem 7: Square Root
	printf("\n\nProblem 7 -------------------------------------------------------------------");
	printf("\nThe square root of 12345 is: %lf.", sqrt(12345.0));
	printf("\nFind the square root of the number: ");
	scanf("%lf", &number);
	printf("The square root of the number %lf is %lf.", number, sqrt(number));

	// Problem 8: Print a Sequence
	printf("\n\nProblem 8 -------------------------------------------------------------------");
	printf("\nEnter the number of elements from the sequence to be printed: n = ");
	scanf("%d", &n);
	element = -1;
	for (i = 0; i < n; i++)
	{
		element = (element > 0)?(abs(element) + 1)*(-1):(abs(element) + 1);
		if ((n - 1) == i) printf("%d", element);
		else printf("%d ", element);
	}

	// Problem 9: Programming Languages
	// See programming-languages.txt

	// Problem 10. Compiled vs Interpreted Languages
	//
	// Wikipedia:
	//
	// An interpreted language is a programming language for which most of its implementations execute 
	// instructions directly, without previously compiling a program into machine-language instructions. 
	// The interpreter executes the program directly, translating each statement into a sequence of one 
	// or more subroutines already compiled into machine code.
	//
	// An interpreted language is a programming language for which most of its implementations execute 
	// instructions directly, without previously compiling a program into machine-language instructions. 
	// The interpreter executes the program directly, translating each statement into a sequence of one 
	// or more subroutines already compiled into machine code.
	//
	// The terms interpreted language and compiled language are not well defined because, in theory, 
	// any programming language can be either interpreted or compiled. In modern programming language 
	// implementation it is increasingly popular for a platform to provide both options.
	//
	// C is commonly considered to be compiled.

	// Problem 11: Development Environments
	// See list-of-IDEs.txt

	printf("\n\n");

	return 0;
}
